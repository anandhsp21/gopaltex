<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliverin extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Company_model', 'company');
        $this->load->model('Employees_model', 'employee');
        $this->load->model('KnottingEntry_model', 'knotting');
        $this->load->model('Orders_model', 'order');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function company_change()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $company_id = $this->input->post('company_id');
            $data['all_company_data'] = $this->order->GetAllOrderRecordsByCompanyId($company_id, '');
            echo json_encode($data['all_company_data']);
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function order_change()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $order_id = $this->input->post('order_id');
            $data['all_order_data'] = $this->order->order_edit_data($order_id);
            echo json_encode($data['all_order_data']);
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $data['all_company_data'] = $this->company->GetAllCompanyRecords();
            $this->load->view('deliverin/deliverin_create', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function insert()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $date = $this->input->post('date');
            $loom_no = $this->input->post('loom_no');
            $emp_id = $this->input->post('emp_id');
            $order_id = $this->input->post('order_id');
            $status = 'notpaid';



            $warp_colour_1 = $this->input->post('warp_colour_1');
            $warp_colour_2 = $this->input->post('warp_colour_2');
            $warp_colour_3 = $this->input->post('warp_colour_3');
            $warp_colour_4 = $this->input->post('warp_colour_4');
            $warp_colour_5 = $this->input->post('warp_colour_5');
            $warp_colour_6 = $this->input->post('warp_colour_6');
            $warp_colour_7 = $this->input->post('warp_colour_7');
            $warp_colour_8 = $this->input->post('warp_colour_8');
            $warp_colour_9 = $this->input->post('warp_colour_9');
            $warp_colour_10 = $this->input->post('warp_colour_10');

            $weft_colour_1 = $this->input->post('weft_colour_1');
            $weft_colour_2 = $this->input->post('weft_colour_2');
            $weft_colour_3 = $this->input->post('weft_colour_3');
            $weft_colour_4 = $this->input->post('weft_colour_4');
            $weft_colour_5 = $this->input->post('weft_colour_5');
            $weft_colour_6 = $this->input->post('weft_colour_6');
            $weft_colour_7 = $this->input->post('weft_colour_7');
            $weft_colour_8 = $this->input->post('weft_colour_8');
            $weft_colour_9 = $this->input->post('weft_colour_9');
            $weft_colour_10 = $this->input->post('weft_colour_10');


            var_dump($weft_colour_1);
            var_dump($weft_colour_2);
            var_dump($weft_colour_3);
            var_dump($weft_colour_4);
            var_dump($weft_colour_5);
            var_dump($weft_colour_6);
            var_dump($weft_colour_7);
            var_dump($weft_colour_8);
            var_dump($weft_colour_9);
            var_dump($weft_colour_10);

            var_dump($warp_colour_1);
            var_dump($warp_colour_2);
            var_dump($warp_colour_3);
            var_dump($warp_colour_4);
            var_dump($warp_colour_5);
            var_dump($warp_colour_6);
            var_dump($warp_colour_7);
            var_dump($warp_colour_8);
            var_dump($warp_colour_9);
            var_dump($warp_colour_10);



            die;

            $data = array(
                'date' => isset($date) ? $date : '',
                'emp_id' => isset($emp_id) ? $emp_id : '',
                'order_id' => isset($order_id) ? $order_id : '',
                'loom_no' => isset($loom_no) ? $loom_no : '',
                'payment_status' => isset($status) ? $status : '',
                'created_at' => date("m/d/y h:i:s")
            );
            if ($data) {
                $insert = $this->knotting->create($data);
                if ($insert) {
                    $this->session->set_flashdata('message', 'Warping Entry Created Successfully');
                    redirect('deliverin/view');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }


    public function CommonViewFn()
    {
        if ($this->input->post('date') != "") {
            $search_date = trim($this->input->post('date'));
        } else {
            $search_date = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
        }
        $data['search_date'] = $search_date;
        $allrecord = $this->knotting->allrecord($search_date);
        $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $search_date;
        $paging = array();
        $paging['base_url'] = $baseurl;
        $paging['total_rows'] = $allrecord;
        $paging['query_string_segment'] = 'per_page';
        $paging['per_page'] = 10;
        $paging['uri_segment'] = 4;
        $paging['num_links'] = 5;
        $paging['first_link'] = 'First';
        $paging['first_tag_open'] = '<li>';
        $paging['first_tag_close'] = '</li>';
        $paging['num_tag_open'] = '<li>';
        $paging['num_tag_close'] = '</li>';
        $paging['prev_link'] = 'Prev';
        $paging['prev_tag_open'] = '<li>';
        $paging['prev_tag_close'] = '</li>';
        $paging['next_link'] = 'Next';
        $paging['next_tag_open'] = '<li>';
        $paging['next_tag_close'] = '</li>';
        $paging['last_link'] = 'Last';
        $paging['last_tag_open'] = '<li>';
        $paging['last_tag_close'] = '</li>';
        $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $paging['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($paging);
        $data['limit'] = $paging['per_page'];
        $data['number_page'] = $paging['per_page'];
        $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
        $data['nav'] = $this->pagination->create_links();
        $data['datas'] = $this->knotting->data_list($data['limit'], $data['offset'], $search_date);
        $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('','');
        $data['orders_ids'] = $this->order->GetAllOrderRecords('');

        $this->load->view('template/header');
        $this->load->view('template/home_login_section');
        $this->load->view('deliverin/deliverin_view', $data, FALSE);
        $this->load->view('template/footer');

    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->knotting->entry_edit_data($id);
            $data['edit_data'] = $result_data[0];
            $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('knitting','');
            $data['orders_ids'] = $this->order->GetAllOrderRecords('');

            $this->load->view('deliverin/deliverin_edit', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $date = $this->input->post('date');
            $loom_no = $this->input->post('loom_no');
            $emp_id = $this->input->post('emp_id');
            $order_id = $this->input->post('order_id');
            $status = 'notpaid';
            $data = array(
                'date' => isset($date) ? $date : '',
                'emp_id' => isset($emp_id) ? $emp_id : '',
                'order_id' => isset($order_id) ? $order_id : '',
                'loom_no' => isset($loom_no) ? $loom_no : '',
                'payment_status' => isset($status) ? $status : '',
                'created_at' => date("m/d/y h:i:s")
            );

            $this->db->where('id', $this->input->post('id'));
            $this->db->update('deliverin_data', $data);
            $this->session->set_flashdata('message', 'Knotting Entry Updated Successfully..');
            redirect('knotting/view');

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('deliverin_data');
            $this->session->set_flashdata('message', 'Knotting deleted Successfully..');
            redirect('knotting/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}