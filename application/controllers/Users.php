<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Users_model', 'users');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section', FALSE);
            $this->load->view('users/users_create');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $data = array(
                'firstname' => trim($this->input->post('firstname')),
                'lastname' => trim($this->input->post('lastname')),
                'username' => trim($this->input->post('username')),
                'email' => trim($this->input->post('email')),
                'sex' => trim($this->input->post('sex')),
                'address' => trim($this->input->post('address')),
                'pass' => md5(trim($this->input->post('password'))),
                'password' => trim($this->input->post('password')),
                'role_id' => '1',
                'created_date' => trim(date("m/d/y h:i:s")),
                'status' => trim($this->input->post('status')));
            $insert = $this->users->create($data);
            if ($insert) {
                $this->session->set_flashdata('message','User Created Successfully');
                redirect('users/view');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            if ($this->input->post('username') != "") {
                $username = trim($this->input->post('username'));
            } else {
                $username = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            }
            $data['search_title'] = $username;
            $allrecord = $this->users->allrecord($username);

            $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $username;
            $paging = array();
            $paging['base_url'] = $baseurl;
            $paging['total_rows'] = $allrecord;
            $paging['query_string_segment'] = 'per_page';
            $paging['per_page'] = 5;
            $paging['uri_segment'] = 4;
            $paging['num_links'] = 5;
            $paging['first_link'] = 'First';
            $paging['first_tag_open'] = '<li>';
            $paging['first_tag_close'] = '</li>';
            $paging['num_tag_open'] = '<li>';
            $paging['num_tag_close'] = '</li>';
            $paging['prev_link'] = 'Prev';
            $paging['prev_tag_open'] = '<li>';
            $paging['prev_tag_close'] = '</li>';
            $paging['next_link'] = 'Next';
            $paging['next_tag_open'] = '<li>';
            $paging['next_tag_close'] = '</li>';
            $paging['last_link'] = 'Last';
            $paging['last_tag_open'] = '<li>';
            $paging['last_tag_close'] = '</li>';
            $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $paging['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($paging);
            $data['limit'] = $paging['per_page'];
            $data['number_page'] = $paging['per_page'];
            $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
            $data['nav'] = $this->pagination->create_links();
            $data['datas'] = $this->users->data_list($data['limit'], $data['offset'], $username);

            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('users/users_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->users->user_edit_data($id);
            $this->data['edit_data'] = $result_data[0];
            $this->load->view('users/users_edit', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $data = array(
                'firstname' => trim($this->input->post('firstname')),
                'lastname' => trim($this->input->post('lastname')),
                'username' => trim($this->input->post('username')),
                'email' => trim($this->input->post('email')),
                'sex' => trim($this->input->post('sex')),
                'address' => trim($this->input->post('address')),
                'pass' => md5(trim($this->input->post('password'))),
                'password' => trim($this->input->post('password')),
                'role_id' => '1',
                'created_date' => trim(date("m/d/y h:i:s")),
                'status' => trim($this->input->post('status')));
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('user_data', $data);
            $this->session->set_flashdata('message', 'User updated successfully..');
            redirect('users/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('user_data');
            $this->session->set_flashdata('message', 'User deleted successfully..');
            redirect('users/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function add_data()
    {
        $this->load->view('template/header');
        $this->load->view('template/add');
        $this->load->view('template/footer');
    }

    public function view_user_data($id)
    {
        $Usersession = array(
            'user_id_page' => $id
        );
        $this->session->set_userdata($Usersession);
        $usr = $id;
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $comment_data = $this->users->users_view_data($usr);
            foreach ($comment_data as $key => $values) {
                $document_id = $values['id'];
                $comment_count = $this->users->comment_count($document_id);
                $likes_counts = $this->users->likes_count_total($document_id);
                $likes = $likes_counts['count'];
                $status = $likes_counts['status'];
                $final_count = $comment_count['COUNT(document_id)'];
                $this->data[$key] = $values;
                $this->data[$key]['count'] = $final_count;
                $this->data[$key]['likes'] = $likes;
                $this->data[$key]['status'] = $status;
            }

            $this->data['view_data'] = @$this->data;
            $this->load->view('admin/view_user_data', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function admin_add_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('admin/users_add');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function admin_submit_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = array(
                'upload_path' => './uploads',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024',
                'max_width' => '1024000',
                'max_height' => '768000',
                'encrypt_name' => true,
            );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('userfile')) {

                $upload_data = $this->upload->data();
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'user_id' => $this->session->userdata('user_id_page'),
                    'document_name' => $upload_data['file_name'],
                    'created_at' => date("Y-m-d")
                );
                $insert = $this->users->document_data($data);
                if ($insert) {
                    $this->session->set_flashdata('message', 'File uploaded Successfully');
                    redirect('users/index');
                }
            } else {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect('users/index');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function admin_users_delete_data($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('users_data');
            $this->session->set_flashdata('message', 'Your data has been deleted Successfully..');
            redirect('users/index');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function edit_data($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->data['edit_data'] = $this->users->edit_data($id);
            $this->load->view('admin/edit', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update_data($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $data = array('username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'sex' => $this->input->post('sex'),
                'address' => $this->input->post('address'),
                'created_date' => date("m/d/y h:i:s"),
                'status' => 'Y');
            $this->db->where('id', $id);
            $this->db->update('user_data', $data);
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            redirect('users/index');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function delete_data($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('user_data');
            $this->session->set_flashdata('message', 'Your data deleted Successfully..');
            redirect('users/index');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function submit_data(){
        $data = array(
            'firstname' => trim($this->input->post('firstname')),
            'lastname' => trim($this->input->post('lastname')),
            'username' => trim($this->input->post('username')),
            'email' => trim($this->input->post('email')),
            'sex' => trim($this->input->post('sex')),
            'address' => trim($this->input->post('address')),
            'pass' => md5(trim($this->input->post('password'))),
            'password' => trim($this->input->post('password')),
            'role_id' => '1',
            'created_date' => trim(date("m/d/y h:i:s")),
            'status' => 'Y');
        $insert = $this->users->insert_data($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('users/index');
    }

    public function login_data()
    {
        $data = array(
            'username' => trim($this->input->post('username')),
            'password' => md5(trim($this->input->post('password')))
        );
        $getdata = $this->users->checkusername($data);
        if (isset($getdata) && is_array($getdata) && !empty($getdata)) {
            $Usersession = array(
                'id' => $getdata[0]['id'],
                'firstname' => $getdata[0]['firstname'],
                'lastname' => $getdata[0]['lastname'],
                'username' => $getdata[0]['username'],
                'email' => $getdata[0]['email'],
                'role_id' => $getdata[0]['role_id'],
                'sex' => $getdata[0]['sex'],
                'address' => $getdata[0]['address']
            );
            $this->session->set_userdata($Usersession);
            $this->session->set_flashdata('message', 'You have been logined Successfully..');
            redirect('users/index');
        } else {
            $this->session->set_flashdata('message', 'Invalid Login details');
            redirect('login');
        }
    }

    public function users()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $comment_data = $this->users->users_view_data($usr);
            foreach ($comment_data as $key => $values) {
                $document_id = $values['id'];
                $user_id = $values['user_id'];
                $comment_count = $this->users->comment_count($document_id);
                $likes_counts = $this->users->likes_count_total($document_id);
                $likes = $likes_counts['count'];
                $status = $likes_counts['status'];
                $final_count = $comment_count['COUNT(document_id)'];
                $this->data[$key] = $values;
                $this->data[$key]['count'] = $final_count;
                $this->data[$key]['likes'] = $likes;
                $this->data[$key]['status'] = $status;
            }

            $this->data['view_data'] = @$this->data;
            $this->load->view('template/users_details', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function send_mail()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'anandh09it@gmail.com', // change it to yours
                'smtp_pass' => 'anandh@123', // change it to yours
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );

            $view_data = $this->users->user_view_data($this->input->post('id'));

            $data = array(
                'username' => $view_data[0]['username'],
                'password' => $view_data[0]['password']
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('anandhhtcdesire@gmail.com'); // change it to yours
            $this->email->to(trim($this->input->post('email')));// change it to yours
            $this->email->subject('Demo application');

            $body = $this->load->view('email/email.php', $data, TRUE);
            $this->email->message($body);

            if ($this->email->send()) {
                echo 'Email sent.';
            } else {
                show_error($this->email->print_debugger());
            }
        }
    }

    public function users_add_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/users_add');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function users_submit_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = array(
                'upload_path' => './uploads',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024',
                'max_width' => '1024000',
                'max_height' => '768000',
                'encrypt_name' => true,
            );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('userfile')) {

                $upload_data = $this->upload->data();
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'user_id' => $this->session->userdata('id'),
                    'document_name' => $upload_data['file_name'],
                    'created_at' => date("Y-m-d")
                );
                $insert = $this->users->document_data($data);
                if ($insert) {
                    $this->session->set_flashdata('message', 'File uploaded Successfully');
                    redirect('users/users');
                }
            } else {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect('users/users');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function users_delete_data($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('users_data');
            $this->session->set_flashdata('message', 'Your data deleted Successfully..');
            redirect('users/users');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function users_comment_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $comment = $this->input->post('comment');
            $data = array(
                'document_id' => trim($this->input->post('id')),
                'user_id' => $this->session->userdata('id'),
                'role_id' => $this->session->userdata('role_id'),
                'comment ' => trim($this->input->post('comment')),
                'status' => 'Y');
            $admin_user_var = $this->session->userdata('role_id');
            $view_data = $this->users->comment_insert_data($data);
            $comment_count = $this->users->comment_count(trim($this->input->post('id')));
            $final_count = $comment_count['COUNT(document_id)'];
            if ($view_data) {
                $result_data = $this->users->getusername($view_data);
                $result_data_array = $result_data[0];
                if ($result_data_array['role_id'] == 0) {
                    $admin_var = 'admin';
                } else {
                    $admin_var = 'user';
                }
                $image_url = $result_data_array['image'];
                $full_url = site_url('uploads/' . $image_url);
                $user_name = $result_data_array['user_name'];
                $comment_data = $result_data_array['comment_data'];
                if ($result_data_array) {
                    $html = array();
                    $html[] = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>"
                        . "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-12 padding_left_null'>"
                        . "<img class='round_image' style = 'width:45px;height:45px;' src = '$full_url' alt = 'image' title = 'image_title'>"
                        . "</div>"
                        . "<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>"
                        . "<h2 class='user_name_bold pull-left'>$user_name</h2>"
                        . "<h2 class='user_name_bold pull-right'>$admin_var</h2>"
                        . "</div>"
                        . "<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>"
                        . "<p>$comment_data</p>"
                        . "</div>"
                        . "</div>";
                    $data_one = array("html" => $html, "count" => $final_count);
                    echo json_encode($data_one);
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function admin_comment_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $comment = $this->input->post('comment');
            $data = array(
                'document_id' => trim($this->input->post('id')),
                'user_id' => $this->session->userdata('user_id_page'),
                'role_id' => $this->session->userdata('role_id'),
                'comment ' => trim($this->input->post('comment')),
                'status' => 'Y');
            $admin_user_var = $this->session->userdata('role_id');
            $view_data = $this->users->comment_insert_data($data);
            $comment_count = $this->users->comment_count(trim($this->input->post('id')));
            $final_count = $comment_count['COUNT(document_id)'];
            if ($view_data) {
                $result_data = $this->users->getusername($view_data);
                $result_data_array = $result_data[0];
                if ($result_data_array['role_id'] == 0) {
                    $admin_var = 'admin';
                } else {
                    $admin_var = 'user';
                }
                $image_url = $result_data_array['image'];
                $full_url = site_url('uploads/' . $image_url);
                $user_name = $result_data_array['user_name'];
                $comment_data = $result_data_array['comment_data'];
                if ($result_data_array) {
                    $html = array();
                    $html = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>"
                        . "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-12 padding_left_null'>"
                        . "<img class='round_image' style = 'width:45px;height:45px;' src = '$full_url' alt = 'image' title = 'image_title'>"
                        . "</div >"
                        . "<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>"
                        . "<h2 class='user_name_bold pull-left' >$user_name</h2>"
                        . "<h2 class='user_name_bold pull-right' >$admin_var</h2>"
                        . "</div>"
                        . "<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>"
                        . "<p>$comment_data</p>"
                        . "</div>"
                        . "</div>";
                    $data_one = array("html" => $html, "count" => $final_count);
                    echo json_encode($data_one);
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function users_like_data()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $id = $this->input->post('id');
            if ($id == TRUE) {
                $id = (int)1;
            }
            $data = array(
                'document_id' => trim($this->input->post('id')),
                'likes_count ' => $id,
                'user_id' => $this->session->userdata('id'),
                'role_id' => $this->session->userdata('role_id'),
                'status ' => 'Y'
            );
            $view_data = $this->users->checking_inserted_data($data);
            if (isset($view_data) && is_array($view_data) && count($view_data)) {
                $view_data_array = $view_data[0];
                if (count($view_data_array) > 1) {
                    $likes_data_id = $view_data_array['id'];
                    $likes_count_num = $view_data_array['likes_count'];
                    $likes_count = '';
                    $status = '';
                    if ($likes_count_num <= 0) {
                        $likes_count = 0;
                    }
                    if ($view_data_array['status'] == 'N') {
                        $status = 'Y';
                        $likes_count = $likes_count_num + 1;
                    }
                    if ($view_data_array['status'] == 'Y') {
                        $status = 'N';
                        $likes_count = $likes_count_num - 1;
                    }
                    $update = $this->users->updating_likes_insert_data($data, $likes_count, $likes_data_id, $status);
                    if ($update == TRUE) {
                        $count_display = $this->users->getting_total_count_likes($this->input->post('id'));
                        $data_one = array("likes_count" => $count_display['count'], "status" => $count_display['status']);
                        echo json_encode($data_one);
                    }
                }
            } else {
                $insert = $this->users->likes_insert_data($data);
                if ($insert == TRUE) {
                    $count_display = $this->users->getting_total_count_likes($this->input->post('id'));
                    $data_one = array("likes_count" => $count_display['count'], "status" => $count_display['status']);
                    echo json_encode($data_one);
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function users_all_comment_data()
    {
        $usr = $this->session->userdata('id');
        $admin_user_var = $this->session->userdata('role_id');
        if (isset($usr) && trim($usr != '')) {
            $document_id = trim($this->input->post('id'));
            if ($document_id) {
                $result_data = $this->users->getallusername($document_id);
                $total_records = count($result_data);
                foreach ($result_data as $result_data_array) {
                    $image_url = $result_data_array['image'];
                    $full_url = site_url('uploads/' . $image_url);
                    $user_name = $result_data_array['user_name'];
                    $comment_data = $result_data_array['comment_data'];
                    if ($result_data_array) {
                        if ($result_data_array['role_id'] == 0) {
                            $admin_var = 'admin';
                        } else {
                            $admin_var = 'user';
                        }
                        $html = '';
                        $html = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>"
                            . "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-12 padding_left_null'>"
                            . "<img class='round_image' style = 'width:45px;height:45px;' src = '$full_url' alt = 'image' title = 'image_title'>"
                            . "</div>"
                            . "<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>"
                            . "<h2 class='user_name_bold pull-left'>$user_name</h2>"
                            . "<h2 class='user_name_bold pull-right'>$admin_var</h2>"
                            . "</div>"
                            . "<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>"
                            . "<p>$comment_data</p>"
                            . "</div>"
                            . "</div>";
                        echo $html;
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('firstname');
        $this->session->unset_userdata('lastname');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('sex');
        $this->session->unset_userdata('address');
        $this->session->set_flashdata('message', 'You have been Successfully logout..');
        redirect('login');
    }

}