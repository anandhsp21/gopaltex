<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entry extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('WeavingEntry_model', 'entry');
        $this->load->model('Employees_model', 'employee');
        $this->load->model('Orders_model', 'order');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {

            $data['date'] = '';
            $data['shift'] = '';
            $data['no_of_entries'] = '';

            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }


    public function weaving_entry_view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {

            $data['date'] = '';
            $data['shift'] = '';
            $data['no_of_entries'] = '';

            $this->load->view('template/header');
            $this->load->view('template/home_login_section');

            $this->load->view('weaving_entry/weaving_entry_create', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function single_create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $weaving_entry = $this->input->post('weaving_entry');
            if ($weaving_entry == 'single') {
                $this->load->view('template/header');
                $this->load->view('template/home_login_section');
                $data['all_employee_data'] = $this->employee->GetAllEmployeeRecords('weaver',true);
                $data['all_order_data'] = $this->order->GetAllOrderRecords(true);
                $this->load->view('weaving_entry/single_weaving_entry_create', $data, FALSE);
                $this->load->view('template/footer');
            } else if ($weaving_entry == 'multiple') {
                $data['date'] = $this->input->post('date');
                $data['shift'] = $this->input->post('shift');
                $data['no_of_entries'] = $this->input->post('no_of_entries');

                $this->load->view('template/header');
                $this->load->view('template/home_login_section');
                $this->load->view('weaving_entry/multiple_weaving_entry_create', $data, FALSE);
                $this->load->view('template/footer');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function multiple_create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $weaving_entry = 'multiple';
            $data['date'] = $this->input->post('date');
            $data['shift'] = $this->input->post('shift');
            $data['no_of_entries'] = $this->input->post('no_of_entries');

            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('weaving_entry/multiple_weaving_entry_create', $data, FALSE);
            $this->load->view('template/footer');

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function single_create_insert()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $weaving_entry = $this->input->post('weaving_entry');
            if ($weaving_entry == 'single') {
                $date = $this->input->post('date');
                $shift = $this->input->post('shift');
                $loom_no = $this->input->post('loom_no');
                $emp_id = $this->input->post('emp_id');
                $order_id = $this->input->post('order_id');
                $meters = $this->input->post('meters');

                $data = array(
                    'date' => isset($date) ? $date : '',
                    'shift' => isset($shift) ? $shift : '',
                    'loom_no' => isset($loom_no) ? $loom_no : '',
                    'emp_id' => isset($emp_id) ? $emp_id : '',
                    'order_id' => isset($order_id) ? $order_id : '',
                    'weaving_entry' => isset($weaving_entry) ? $weaving_entry : '',
                    'meters' => isset($meters) ? $meters : '',
                    'created_at' => date("m/d/y h:i:s")
                );
                if ($data) {
                    $insert = $this->entry->create($data);
                    if ($insert) {
                        $this->session->set_flashdata('message', 'Single Weaving Entry Created Successfully');
                        redirect('entry/view');
                    }
                }
            }

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function multiple_create_insert_loop()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $weaving_entry = $this->input->post('weaving_entry');
            if ($weaving_entry == 'multiple') {
                $data['date'] = $this->input->post('date');
                $data['shift'] = $this->input->post('shift');
                $data['no_of_entries'] = $this->input->post('no_of_entries');
                $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('weaver',true);
                $data['orders_ids'] = $this->order->GetAllOrderRecords(true);
                if($data){
                    $this->load->view('template/header');
                    $this->load->view('template/home_login_section');
                    $this->load->view('weaving_entry/multiple_weaving_entry_create_loop', $data, FALSE);
                    $this->load->view('template/footer');
                }
            }

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function multiple_create_insert()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $weaving_entry = $this->input->post('weaving_entry');
            if ($weaving_entry == 'multiple') {
                $dates = $this->input->post('date');
                $shift = $this->input->post('shift');
                $loom_no = $this->input->post('loom_no');
                $emp_id = $this->input->post('emp_id');
                $order_id = $this->input->post('order_id');
                $meters = $this->input->post('meters');
                if ($dates && $shift && $loom_no && $emp_id && $order_id && $meters) {
                    for ($i = 0; $i < sizeof($dates); $i++) {
                        $collection[$i] = array(
                            'loom_no' => trim($loom_no[$i]),
                            'emp_id' => trim($emp_id[$i]),
                            'order_id' => trim($order_id[$i]),
                            'meters' => trim($meters[$i]),
                            'date' => trim($dates[$i]),
                            'shift' => trim($shift[$i]),
                            'weaving_entry' => trim($weaving_entry),
                            'created_at' => trim(date("m/d/y h:i:s"))
                        );
                    }
                    $insert = $this->entry->insert_all($collection);
                    if ($insert) {
                        $this->session->set_flashdata('message', 'Multiple Weaving Entry Created Successfully');
                        redirect('entry/view');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }


    public function CommonViewFn()
    {
        if ($this->input->post('date') != "") {
            $search_date = trim($this->input->post('date'));
        } else {
            $search_date = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
        }
        $data['search_date'] = $search_date;
        $allrecord = $this->entry->allrecord($search_date);
        $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $search_date;
        $paging = array();
        $paging['base_url'] = $baseurl;
        $paging['total_rows'] = $allrecord;
        $paging['query_string_segment'] = 'per_page';
        $paging['per_page'] = 10;
        $paging['uri_segment'] = 4;
        $paging['num_links'] = 5;
        $paging['first_link'] = 'First';
        $paging['first_tag_open'] = '<li>';
        $paging['first_tag_close'] = '</li>';
        $paging['num_tag_open'] = '<li>';
        $paging['num_tag_close'] = '</li>';
        $paging['prev_link'] = 'Prev';
        $paging['prev_tag_open'] = '<li>';
        $paging['prev_tag_close'] = '</li>';
        $paging['next_link'] = 'Next';
        $paging['next_tag_open'] = '<li>';
        $paging['next_tag_close'] = '</li>';
        $paging['last_link'] = 'Last';
        $paging['last_tag_open'] = '<li>';
        $paging['last_tag_close'] = '</li>';
        $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $paging['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($paging);
        $data['limit'] = $paging['per_page'];
        $data['number_page'] = $paging['per_page'];
        $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
        $data['nav'] = $this->pagination->create_links();
        $data['datas'] = $this->entry->data_list($data['limit'], $data['offset'], $search_date);
        $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('weaver','');
        $data['orders_ids'] = $this->order->GetAllOrderRecords('');

//        var_dump($data['orders_ids']);die;

        $this->load->view('template/header');
        $this->load->view('template/home_login_section');
        $this->load->view('weaving_entry/weaving_entry_view', $data, FALSE);
        $this->load->view('template/footer');
    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->entry->entry_edit_data($id);
            $data['edit_data'] = $result_data[0];
            $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('weaver','');

//            var_dump($data['employee_ids']);

            $data['orders_ids'] = $this->order->GetAllOrderRecords('');
            $this->load->view('weaving_entry/weaving_entry_edit', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $weaving_entry = $this->input->post('weaving_entry');
            if ($weaving_entry == 'single') {
                $date = $this->input->post('date');
                $shift = $this->input->post('shift');
                $loom_no = $this->input->post('loom_no');
                $emp_id = $this->input->post('emp_id');
                $order_id = $this->input->post('order_id');
                $meters = $this->input->post('meters');
                $data = array(
                    'date' => isset($date) ? $date : '',
                    'shift' => isset($shift) ? $shift : '',
                    'loom_no' => isset($loom_no) ? $loom_no : '',
                    'emp_id' => isset($emp_id) ? $emp_id : '',
                    'order_id' => isset($order_id) ? $order_id : '',
                    'weaving_entry' => isset($weaving_entry) ? $weaving_entry : '',
                    'meters' => isset($meters) ? $meters : '',
                    'created_at' => date("m/d/y h:i:s")
                );
            }
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('weaving_entry_data', $data);
            $this->session->set_flashdata('message', 'Entry Updated Successfully..');
            redirect('entry/view');

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('weaving_entry_data');
            $this->session->set_flashdata('message', 'Entry deleted Successfully..');
            redirect('entry/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

}