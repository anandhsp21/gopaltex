<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wage extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('PinEntry_model', 'pin');
        $this->load->model('Employees_model', 'employee');
        $this->load->model('Orders_model', 'order');
        $this->load->model('Wage_Payment_model', 'wage');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $data['all_employee_data'] = $this->employee->GetAllEmployeeRecords('', '');
            $data['all_order_data'] = $this->order->GetAllOrderRecords('');
            $this->load->view('wage_payment/wage_payment_create', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function invoice()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $search_dates = trim($this->input->post('date'));
            $search_date = isset($search_dates) ? $search_dates : '';
            $emp_id = trim($this->input->post('emp_id'));
            $employee_id = '';
            if ($search_date) {
                $employee_id = '';
            } else {
                if ($emp_id) {
                    $employee_id = $emp_id;
                }
                $search_date = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            }
            $data['search_date'] = $search_date;
            $data['selected_id'] = $employee_id;
            $allrecord = $this->wage->pdfallrecord($search_date, $employee_id);
            $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $search_date;
            $paging = array();
            $paging['base_url'] = $baseurl;
            $paging['total_rows'] = $allrecord;
            $paging['query_string_segment'] = 'per_page';
            $paging['per_page'] = 10;
            $paging['uri_segment'] = 4;
            $paging['num_links'] = 5;
            $paging['first_link'] = 'First';
            $paging['first_tag_open'] = '<li>';
            $paging['first_tag_close'] = '</li>';
            $paging['num_tag_open'] = '<li>';
            $paging['num_tag_close'] = '</li>';
            $paging['prev_link'] = 'Prev';
            $paging['prev_tag_open'] = '<li>';
            $paging['prev_tag_close'] = '</li>';
            $paging['next_link'] = 'Next';
            $paging['next_tag_open'] = '<li>';
            $paging['next_tag_close'] = '</li>';
            $paging['last_link'] = 'Last';
            $paging['last_tag_open'] = '<li>';
            $paging['last_tag_close'] = '</li>';
            $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $paging['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($paging);
            $data['limit'] = $paging['per_page'];
            $data['number_page'] = $paging['per_page'];
            $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
            $data['nav'] = $this->pagination->create_links();
            $data['datas'] = $this->wage->pdf_data_list($data['limit'], $data['offset'], $search_date, $employee_id);
            $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('', '');
            $this->load->view('view_invoice/view_invoice_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view_pdf($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $selected_pdf_table = $this->wage->getDataFromPdfTable($id);
            $selected_pdf_row = $selected_pdf_table[0];
            $file_name = 'pdf_files/' . $selected_pdf_row['pdf_name'] . '.pdf';
            if (file_exists($file_name)) {
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $file_name . '"');
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: ' . filesize($file_name));
                header('Accept-Ranges: bytes');
                @readfile($file_name);
            } else {
                $this->session->set_flashdata('message', 'No Pdf File For The User..');
                redirect('wage/invoice');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function cancel_pdf($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {

            $selected_pdf_table = $this->wage->getDataFromPdfTable($id);

            $selected_pdf_row = $selected_pdf_table[0];
            $cbalances = $selected_pdf_row['balance_value'];
            $emp_id = $selected_pdf_row['emp_id'];
            $inter_values = unserialize($selected_pdf_row['inter_value']);
            $weaving_status_collection = unserialize($selected_pdf_row['weaving_value']);
            $pdf_status = $selected_pdf_row['status'];

            if ($pdf_status == 'cancelled') {
                $this->session->set_flashdata('message', 'Pdf Already Cancelled');
                redirect('wage/invoice');
            } else {
                /*balance update*/
                if (isset($cbalances)) {
                    $update_data = array(
                        'balance' => isset($cbalances) ? $cbalances : '',
                    );
                    $this->db->where('id', $emp_id);
                    $this->db->update('employee_data', $update_data);
                }

                /*status change in weaving table*/
                if (isset($weaving_status_collection) && count($weaving_status_collection) > 0) {
                    foreach ($weaving_status_collection as $weaving_status) {
                        $update_weaving_data = array(
                            'payment_status' => 'notpaid',
                        );
                        $this->db->where('id', $weaving_status);
                        $this->db->update('weaving_entry_data', $update_weaving_data);
                    }
                }

                /*status change in intermediate table*/
                if (isset($inter_values) && count($inter_values) > 0) {
                    foreach ($inter_values as $inter_value) {
                        $inter_weaving_data = array(
                            'status' => 'not_tallied',
                        );
                        $this->db->where('id', $inter_value);
                        $this->db->update('intermediate_payment_data', $inter_weaving_data);
                    }
                }

                /*Delete weekly payment table row*/
                if ($id) {
                    $this->db->where('id', $id);
                    $this->db->delete('weekly_payment_data');
                }

                /*Update pdf table as cancelled*/
                if ($id) {
                    $update_pdf_data = array(
                        'status' => 'cancelled',
                    );
                    $this->db->where('id', $id);
                    $this->db->update('pdf_rollback_data', $update_pdf_data);
                }

                /*redirect to invoice page*/
                $this->session->set_flashdata('message', 'Pdf Cancelled Successfully');
                redirect('wage/invoice');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $data['all_employee_data'] = $this->employee->GetAllEmployeeRecords('', '');
            $data['all_order_data'] = $this->order->GetAllOrderRecords('');
            $this->load->view('wage_payment/wage_payment_create', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function desig_change()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $desig = $this->input->post('desig');
            $data['all_employee_data'] = $this->employee->GetAllEmployeeRecords($desig, '');
            echo json_encode($data['all_employee_data']);
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function calculate()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $desig = $this->input->post('desig');
            $emp_id = $this->input->post('emp_id');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $payment_status = $this->input->post('payment_status');
            $data_coll = $this->calculate_wage_common($desig, $emp_id, $from_date, $to_date, $payment_status);
            if ($data_coll) {
                $this->load->view('wage_payment/wage_payment_view', $data_coll, FALSE);
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function calculate_wage_common($desig, $emp_id, $from_date, $to_date, $payment_status)
    {
        $insert_data = array(
            'desig' => isset($desig) ? $desig : '',
            'emp_id' => isset($emp_id) ? $emp_id : '',
            'from_date' => isset($from_date) ? $from_date : '',
            'to_date' => isset($to_date) ? $to_date : '',
            'payment_status' => isset($payment_status) ? $payment_status : 'notpaid'
        );
        switch ($desig) {
            case "weaver":
                $db_name = 'weaving_entry_data';
                $col_name = 'loom_cost';
                break;
            case "warping":
                $db_name = 'warping_entry_data';
                $col_name = 'warp_cost';
                break;
            case "winding":
                $db_name = 'winding_entry_data';
                $col_name = 'warp_cost';
                break;
            case "hooking":
                $db_name = 'hook_entry_data';
                $col_name = 'hook_cost';
                break;
            case "knitting":
                $db_name = 'knotting_entry_data';
                $col_name = 'knot_cost';
                break;
            case "pinner":
                $db_name = 'pin_entry_data';
                $col_name = 'pin_cost';
                break;
            case "fitter":
                $db_name = 'warping_entry_data';
                $col_name = 'pin_cost';
                break;
            case "manager":
                $db_name = 'warping_entry_data';
                $col_name = 'pin_cost';
                break;
            default:
                echo "test";
        }
        if ($insert_data) {
            $data['datas'] = $this->wage->calculate_wage($insert_data, $db_name);
            $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('', '');
            $data['orders_ids'] = $this->order->GetAllOrderRecords('');
            $emp_datas = $this->wage->getEmpNameFromId($emp_id);
            $inter_value = $this->wage->getInterValueFromId($emp_id);
            $prev_balance = $this->wage->getPrevBalanceFromId($emp_id);

            if ($emp_datas && $emp_datas[0]['emp_name']) {
                $emp_name = $emp_datas[0]['emp_name'];
            } else {
                $emp_name = '';
            }

            if ($prev_balance && $prev_balance[0]['balance']) {
                $prev_bal = $prev_balance[0]['balance'];
            } else {
                $prev_bal = '';
            }

            $final_amount = 0;

            if (isset($inter_value) && count($inter_value) > 0) {
                foreach ($inter_value as $value) {
                    $amount = $value['amount'];
                    $final_amount += $amount;
                }
            }

            $data['emp_name'] = $emp_name;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            $data['db_name'] = $db_name;
            $data['desig'] = $desig;
            $data['col_name'] = $col_name;
            $data['emp_id'] = $emp_id;
            $data['intermediate_payment'] = $final_amount;
            $data['prev_balance'] = $prev_bal;
            return $data;
        }
    }


    public function calculate_wage()
    {
        $usr = $this->session->userdata('id');
        $store = array();
        if (isset($usr) && trim($usr != '')) {
            $cbalances = $this->input->post('cbalances');
            $paid_amount = $this->input->post('paid_amount');
            $extras = $this->input->post('extras');
            $desig = $this->input->post('desig');
            $final_cost = $this->input->post('final_cost');
            $intermediate_payment = $this->input->post('intermediate_payment');
            $prev_balance = $this->input->post('prev_balance');
            $emp_id = $this->input->post('emp_id');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $payment_status = 'notpaid';

            $datas = array(
                'cbalances' => isset($cbalances) ? $cbalances : 0,
                'paid_amount' => isset($paid_amount) ? $paid_amount : 0,
                'extras' => isset($extras) ? $extras : 0,
                'final_cost' => isset($final_cost) ? $final_cost : 0,
                'intermediate_payment' => isset($intermediate_payment) ? $intermediate_payment : 0,
                'prev_balance' => isset($prev_balance) ? $prev_balance : 0,
                'emp_id' => isset($emp_id) ? $emp_id : '',
                'from_date' => isset($from_date) ? $from_date : '',
                'to_date' => isset($to_date) ? $to_date : '',
                'payment_status' => isset($payment_status) ? $payment_status : '',
                'created_at' => date("m/d/y h:i:s")
            );

            /*balance update*/
            if ($cbalances) {
                $update_data = array(
                    'balance' => isset($cbalances) ? $cbalances : '',
                );
                $this->db->where('id', $emp_id);
                $this->db->update('employee_data', $update_data);
            }

            /*status change in intermediate tab*/
            $inter_value_ids = array();
            $inter_values = $this->wage->getInterValueFromId($emp_id);
            if (isset($inter_values) && count($inter_values) > 0) {
                foreach ($inter_values as $inter_value) {
                    $inter_value_id = $inter_value['id'];
                    $inter_value_ids[] = $inter_value['id'];
                    $inter_weaving_data = array(
                        'status' => 'tallied',
                    );
                    $this->db->where('id', $inter_value_id);
                    $this->db->update('intermediate_payment_data', $inter_weaving_data);
                }
                $serial_inter_value_ids = serialize($inter_value_ids);
            }

            $weekly_payment_insert = $this->wage->weekly_payment_data_create($datas);

            if ($weekly_payment_insert) {

                if ($emp_id && $desig) {

                    $values = $this->calculate_wage_common($desig, $emp_id, $from_date, $to_date, $payment_status);

                    switch ($desig) {
                        case "weaver":
                            $db_name = 'weaving_entry_data';
                            break;
                        case "warping":
                            $db_name = 'warping_entry_data';
                            break;
                        case "winding":
                            $db_name = 'winding_entry_data';
                            break;
                        case "hooking":
                            $db_name = 'hook_entry_data';
                            break;
                        case "knitting":
                            $db_name = 'knotting_entry_data';
                            break;
                        case "pinner":
                            $db_name = 'pin_entry_data';
                            break;
                        case "fitter":
                            $db_name = 'warping_entry_data';
                            break;
                        case "manager":
                            $db_name = 'warping_entry_data';
                            break;
                        default:
                            echo "test";
                    }

                    /*status change in depends upon design entry tab*/
                    $weaving_status_collection = $this->wage->calculate_wage($datas, $db_name);
                    $weaving_status_ids = array();

                    if (isset($weaving_status_collection) && count($weaving_status_collection) > 0) {
                        foreach ($weaving_status_collection as $weaving_status) {
                            $weaving_status_id = $weaving_status['id'];
                            $weaving_status_ids[] = $weaving_status['id'];
                            $update_weaving_data = array(
                                'payment_status' => 'Paid',
                            );
                            $this->db->where('id', $weaving_status_id);
                            $this->db->update($db_name, $update_weaving_data);
                        }
                        $serial_weaving_status_ids = serialize($weaving_status_ids);
                    }

                    if ($values) {
                        $values['cbalances'] = isset($cbalances) ? $cbalances : 0;
                        $values['paid_amount'] = isset($paid_amount) ? $paid_amount : 0;
                        $values['extras'] = isset($extras) ? $extras : 0;
                        $values['prev_balance'] = isset($prev_balance) ? $prev_balance : 0;
                        $values['intermediate_payment'] = isset($intermediate_payment) ? $intermediate_payment : 0;

                        /*Update it if same id*/
                        /* $emp_id_already = $this->wage->getEmpIdFromPdfTable($emp_id);
                         $pdf_id = $emp_id_already[0]['id'];
                         if (isset($emp_id_already) && count($emp_id_already) > 0) {
                             $this->db->where('id', $pdf_id);
                             $this->db->update('pdf_rollback_data', $store);
                         }*/

                        $pdf_name = strtolower(trim($values['emp_name'])) . '-' . date("d-m-Y") . "-" . time();

                        $store['emp_id'] = $emp_id;
                        $store['balance_value'] = isset($prev_balance) ? $prev_balance : 0;
                        $store['inter_value'] = isset($serial_inter_value_ids) ? $serial_inter_value_ids : '';
                        $store['weekly_payment_value'] = isset($weekly_payment_insert) ? $weekly_payment_insert : '';
                        $store['weaving_value'] = isset($serial_weaving_status_ids) ? $serial_weaving_status_ids : '';
                        $store['date'] = date("d-m-Y");
                        $store['pdf_name'] = isset($pdf_name) ? $pdf_name : '';
                        $store['status'] = 'active';
                        $store['created_at'] = date("m/d/y h:i:s");

                        $serial_no = $this->wage->pdf_data_create($store);
                        $values['pdf_id'] = $serial_no;
                        if ($store) {
                            $html = $this->load->view('wage_payment/wage_pdf_payment_view', $values, TRUE);
                            $mpdf = new \Mpdf\Mpdf();
                            $mpdf->WriteHTML($html);
                            $mpdf->Output('./pdf_files/' . $pdf_name . '.pdf', 'F');
                            $this->session->set_flashdata('message', 'Payment Successful');
                            redirect('wage/invoice');
                        }
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }


    public function CommonViewFn()
    {
        if ($this->input->post('date') != "") {
            $search_date = trim($this->input->post('date'));
        } else {
            $search_date = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
        }
        $data['search_date'] = $search_date;
        $allrecord = $this->wage->allrecord($search_date);
        $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $search_date;
        $paging = array();
        $paging['base_url'] = $baseurl;
        $paging['total_rows'] = $allrecord;
        $paging['query_string_segment'] = 'per_page';
        $paging['per_page'] = 10;
        $paging['uri_segment'] = 4;
        $paging['num_links'] = 5;
        $paging['first_link'] = 'First';
        $paging['first_tag_open'] = '<li>';
        $paging['first_tag_close'] = '</li>';
        $paging['num_tag_open'] = '<li>';
        $paging['num_tag_close'] = '</li>';
        $paging['prev_link'] = 'Prev';
        $paging['prev_tag_open'] = '<li>';
        $paging['prev_tag_close'] = '</li>';
        $paging['next_link'] = 'Next';
        $paging['next_tag_open'] = '<li>';
        $paging['next_tag_close'] = '</li>';
        $paging['last_link'] = 'Last';
        $paging['last_tag_open'] = '<li>';
        $paging['last_tag_close'] = '</li>';
        $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $paging['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($paging);
        $data['limit'] = $paging['per_page'];
        $data['number_page'] = $paging['per_page'];
        $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
        $data['nav'] = $this->pagination->create_links();
        $data['datas'] = $this->wage->data_list($data['limit'], $data['offset'], $search_date);

        $this->load->view('template/header');
        $this->load->view('template/home_login_section');
        $this->load->view('wage_payment/wage_payment_view', $data, FALSE);
        $this->load->view('template/footer');

    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->wage->entry_edit_data($id);
            $data['edit_data'] = $result_data[0];
            $data['all_order_data'] = $this->order->GetAllOrderRecords('');
            $data['all_employee_data'] = $this->employee->GetAllEmployeeRecords('', '');
            $this->load->view('wage_payment/wage_payment_edit', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $date = $this->input->post('date');
            $loom_no = $this->input->post('loom_no');
            $emp_name = $this->input->post('emp_name');
            $order_name = $this->input->post('order_name');
            $status = $this->input->post('status');
            $data = array(
                'date' => isset($date) ? $date : '',
                'emp_name' => isset($emp_name) ? $emp_name : '',
                'order_name' => isset($order_name) ? $order_name : '',
                'loom_no' => isset($loom_no) ? $loom_no : '',
                'status' => isset($status) ? $status : '',
                'created_at' => date("m/d/y h:i:s")
            );

            $this->db->where('id', $this->input->post('id'));
            $this->db->update('wage_payment_data', $data);
            $this->session->set_flashdata('message', 'Wage Entry Updated Successfully..');
            redirect('wage/view');

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('wage_payment_data');
            $this->session->set_flashdata('message', 'Wage deleted Successfully..');
            redirect('wage/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}