<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Orders_model', 'orders');
        $this->load->model('Company_model', 'company');
        $this->load->model('Employees_model', 'employee');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $data['all_company_data'] = $this->company->GetAllCompanyRecords();
            $this->load->view('orders/orders_create', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = array(
                'upload_path' => './uploads/orders',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('userfile')) {
                $upload_data = $this->upload->data();
                $order_image = $upload_data['file_name'];
            } else {
                $order_image = '';
            }

            $order_name = $this->input->post('order_name');
            $company_id = $this->input->post('company_id');
            $order_qty = $this->input->post('order_qty');
            $reed_pick = $this->input->post('reed_pick');
            $cost = $this->input->post('cost');
            $warp_count = $this->input->post('warp_count');
            $warp_colours = $this->input->post('warp_colours');

            $warp_colour_1 = $this->input->post('warp_colour_1');
            $warp_colour_2 = $this->input->post('warp_colour_2');
            $warp_colour_3 = $this->input->post('warp_colour_3');
            $warp_colour_4 = $this->input->post('warp_colour_4');
            $warp_colour_5 = $this->input->post('warp_colour_5');
            $warp_colour_6 = $this->input->post('warp_colour_6');
            $warp_colour_7 = $this->input->post('warp_colour_7');
            $warp_colour_8 = $this->input->post('warp_colour_8');
            $warp_colour_9 = $this->input->post('warp_colour_9');
            $warp_colour_10 = $this->input->post('warp_colour_10');

            $loom_cost = $this->input->post('loom_cost');
            $warp_cost = $this->input->post('warp_cost');
            $knot_cost = $this->input->post('knot_cost');
            $hook_cost = $this->input->post('hook_cost');
            $pin_cost = $this->input->post('pin_cost');
            $weft_count = $this->input->post('weft_count');
            $weft_colours = $this->input->post('weft_colours');

            $weft_colour_1 = $this->input->post('weft_colour_1');
            $weft_colour_2 = $this->input->post('weft_colour_2');
            $weft_colour_3 = $this->input->post('weft_colour_3');
            $weft_colour_4 = $this->input->post('weft_colour_4');
            $weft_colour_5 = $this->input->post('weft_colour_5');
            $weft_colour_6 = $this->input->post('weft_colour_6');
            $weft_colour_7 = $this->input->post('weft_colour_7');
            $weft_colour_8 = $this->input->post('weft_colour_8');
            $weft_colour_9 = $this->input->post('weft_colour_9');
            $weft_colour_10 = $this->input->post('weft_colour_10');

            $status = $this->input->post('status');

            $data = array(
                'order_name' => isset($order_name) ? $order_name : '',
                'company_id' => isset($company_id) ? $company_id : '',
                'order_qty' => isset($order_qty) ? $order_qty : '',
                'reed_pick' => isset($reed_pick) ? $reed_pick : '',
                'cost' => isset($cost) ? $cost : '',
                'warp_count' => isset($warp_count) ? $warp_count : '',
                'warp_colours' => isset($warp_colours) ? $warp_colours : '',

                'warp_colour_1' => isset($warp_colour_1) ? $warp_colour_1 : '',
                'warp_colour_2' => isset($warp_colour_2) ? $warp_colour_2 : '',
                'warp_colour_3' => isset($warp_colour_3) ? $warp_colour_3 : '',
                'warp_colour_4' => isset($warp_colour_4) ? $warp_colour_4 : '',
                'warp_colour_5' => isset($warp_colour_5) ? $warp_colour_5 : '',
                'warp_colour_6' => isset($warp_colour_6) ? $warp_colour_6 : '',
                'warp_colour_7' => isset($warp_colour_7) ? $warp_colour_7 : '',
                'warp_colour_8' => isset($warp_colour_8) ? $warp_colour_8 : '',
                'warp_colour_9' => isset($warp_colour_9) ? $warp_colour_9 : '',
                'warp_colour_10' => isset($warp_colour_10) ? $warp_colour_10 : '',

                'loom_cost' => isset($loom_cost) ? $loom_cost : '',
                'warp_cost' => isset($warp_cost) ? $warp_cost : '',
                'knot_cost' => isset($knot_cost) ? $knot_cost : '',
                'hook_cost' => isset($hook_cost) ? $hook_cost : '',
                'pin_cost' => isset($pin_cost) ? $pin_cost : '',
                'weft_count' => isset($weft_count) ? $weft_count : '',
                'weft_colours' => isset($weft_colours) ? $weft_colours : '',

                'weft_colour_1' => isset($weft_colour_1) ? $weft_colour_1 : '',
                'weft_colour_2' => isset($weft_colour_2) ? $weft_colour_2 : '',
                'weft_colour_3' => isset($weft_colour_3) ? $weft_colour_3 : '',
                'weft_colour_4' => isset($weft_colour_4) ? $weft_colour_4 : '',
                'weft_colour_5' => isset($weft_colour_5) ? $weft_colour_5 : '',
                'weft_colour_6' => isset($weft_colour_6) ? $weft_colour_6 : '',
                'weft_colour_7' => isset($weft_colour_7) ? $weft_colour_7 : '',
                'weft_colour_8' => isset($weft_colour_8) ? $weft_colour_8 : '',
                'weft_colour_9' => isset($weft_colour_9) ? $weft_colour_9 : '',
                'weft_colour_10' => isset($weft_colour_10) ? $weft_colour_10 : '',

                'status' => $status,
                'order_image' => $order_image,
                'created_at' => date("m/d/y h:i:s")
            );

            if ($data) {
                $insert = $this->orders->create($data);
                if ($insert) {
                    $this->session->set_flashdata('message', $this->upload->display_errors() . ' Order Created Successfully');
                    redirect('orders/view');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            if ($this->input->post('order_name') != "") {
                $order_name = trim($this->input->post('order_name'));
            } else {
                $order_name = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            }
            $data['order_title'] = $order_name;
            $allrecord = $this->orders->allrecord($order_name);
            $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $order_name;
            $paging = array();
            $paging['base_url'] = $baseurl;
            $paging['total_rows'] = $allrecord;
            $paging['query_string_segment'] = 'per_page';
            $paging['per_page'] = 10;
            $paging['uri_segment'] = 4;
            $paging['num_links'] = 5;
            $paging['first_link'] = 'First';
            $paging['first_tag_open'] = '<li>';
            $paging['first_tag_close'] = '</li>';
            $paging['num_tag_open'] = '<li>';
            $paging['num_tag_close'] = '</li>';
            $paging['prev_link'] = 'Prev';
            $paging['prev_tag_open'] = '<li>';
            $paging['prev_tag_close'] = '</li>';
            $paging['next_link'] = 'Next';
            $paging['next_tag_open'] = '<li>';
            $paging['next_tag_close'] = '</li>';
            $paging['last_link'] = 'Last';
            $paging['last_tag_open'] = '<li>';
            $paging['last_tag_close'] = '</li>';
            $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $paging['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($paging);
            $data['limit'] = $paging['per_page'];
            $data['number_page'] = $paging['per_page'];
            $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
            $data['nav'] = $this->pagination->create_links();
            $data['datas'] = $this->orders->data_list($data['limit'], $data['offset'], $order_name);
            $data['company_ids'] = $this->company->GetAllCompanyRecords();
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('orders/orders_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function report()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $data['company_ids'] = $this->company->GetAllCompanyRecords();
            $data['datas'] = $this->orders->GetAllOrderPdfRecords();
            $data['title'] = 'Order Report';
            $html = $this->load->view('orders/orders_pdf_view', $data, TRUE);
            $pdf_name = 'order';
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output($pdf_name . '.pdf', 'I');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function detailed_report()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $data['all_employee_data'] = $this->employee->GetAllEmployeeRecords('weaver', true);
            $data['all_order_data'] = $this->orders->GetAllOrderRecords(false);
            $data['title'] = 'Detailed Order Report';
            $this->load->view('orders/detailed_orders_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function detailed_report_pdf()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $order_id = $this->input->post('order_id');
            $pdf_order_ses = array();
            if ($order_id) {
                $pdf_order_ses = array(
                    'order_id' => $order_id
                );
            }
            $this->session->set_userdata($pdf_order_ses);
            $or_id = $this->session->userdata('order_id');
            if (isset($or_id) && !$order_id) {
                $order_id = $or_id;
            }
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            if ($this->input->post('date') != "") {
                $search_date = trim($this->input->post('date'));
            } else {
                $search_date = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            }
            $data['search_date'] = $search_date;
            $data['company_ids'] = $this->company->GetAllCompanyRecords();
            $GetRecordByOrderId = $this->orders->GetRecordByOrderId($order_id);
            $order_name = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            $allrecord = $this->orders->GetRecordByOrderIdNumberRows($order_id, $search_date);
            $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $order_name;
            $paging = array();
            $paging['base_url'] = $baseurl;
            $paging['total_rows'] = $allrecord;
            $paging['query_string_segment'] = 'per_page';
            $paging['per_page'] = 10;
            $paging['uri_segment'] = 4;
            $paging['num_links'] = 5;
            $paging['first_link'] = 'First';
            $paging['first_tag_open'] = '<li>';
            $paging['first_tag_close'] = '</li>';
            $paging['num_tag_open'] = '<li>';
            $paging['num_tag_close'] = '</li>';
            $paging['prev_link'] = 'Prev';
            $paging['prev_tag_open'] = '<li>';
            $paging['prev_tag_close'] = '</li>';
            $paging['next_link'] = 'Next';
            $paging['next_tag_open'] = '<li>';
            $paging['next_tag_close'] = '</li>';
            $paging['last_link'] = 'Last';
            $paging['last_tag_open'] = '<li>';
            $paging['last_tag_close'] = '</li>';
            $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $paging['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($paging);
            $data['limit'] = $paging['per_page'];
            $data['number_page'] = $paging['per_page'];
            $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
            $data['nav'] = $this->pagination->create_links();
            $final_metres = 0;
            foreach ($GetRecordByOrderId as $metres_value) {
                $final_metres += intval($metres_value["meters"]);
            }
            $data['final_metres'] = $final_metres;
            $data['datas'] = $this->orders->pdf_data_list($data['limit'], $data['offset'], $order_id, $search_date);
            $this->load->view('orders/detailed_orders_pdf_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function download_detailed_report_pdf()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $order_id = '';
            $or_id = $this->session->userdata('order_id');
            if (isset($or_id)) {
                $order_id = $or_id;
            }
            $data['datas'] = $this->orders->GetRecordByOrderId($order_id);
            $order_info = $this->orders->get_com_order_name_by_order_id($order_id);
            $data['company_order_name'] = $order_info[0];
            $data['title'] = 'Detailed order Report';
            $html = $this->load->view('orders/download_detailed_orders_pdf_view', $data, TRUE);
            $pdf_name = 'Detailed_order';
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output($pdf_name . '.pdf', 'I');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->orders->order_edit_data($id);
            $this->data['all_company_data'] = $this->company->GetAllCompanyRecords();
            $this->data['edit_data'] = $result_data[0];
            $this->load->view('orders/orders_edit', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = array(
                'upload_path' => './uploads/orders',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('userfile')) {
                $upload_data = $this->upload->data();
                $order_image = $upload_data['file_name'];
            } else {
                $order_image = $this->input->post('UserImageFile');
            }

            $order_name = $this->input->post('order_name');
            $company_id = $this->input->post('company_id');
            $order_qty = $this->input->post('order_qty');
            $reed_pick = $this->input->post('reed_pick');
            $cost = $this->input->post('cost');
            $warp_count = $this->input->post('warp_count');
            $warp_colours = $this->input->post('warp_colours');

            $warp_colour_1 = $this->input->post('warp_colour_1');
            $warp_colour_2 = $this->input->post('warp_colour_2');
            $warp_colour_3 = $this->input->post('warp_colour_3');
            $warp_colour_4 = $this->input->post('warp_colour_4');
            $warp_colour_5 = $this->input->post('warp_colour_5');
            $warp_colour_6 = $this->input->post('warp_colour_6');
            $warp_colour_7 = $this->input->post('warp_colour_7');
            $warp_colour_8 = $this->input->post('warp_colour_8');
            $warp_colour_9 = $this->input->post('warp_colour_9');
            $warp_colour_10 = $this->input->post('warp_colour_10');

            $loom_cost = $this->input->post('loom_cost');
            $warp_cost = $this->input->post('warp_cost');
            $knot_cost = $this->input->post('knot_cost');
            $hook_cost = $this->input->post('hook_cost');
            $pin_cost = $this->input->post('pin_cost');
            $weft_count = $this->input->post('weft_count');
            $weft_colours = $this->input->post('weft_colours');

            $weft_colour_1 = $this->input->post('weft_colour_1');
            $weft_colour_2 = $this->input->post('weft_colour_2');
            $weft_colour_3 = $this->input->post('weft_colour_3');
            $weft_colour_4 = $this->input->post('weft_colour_4');
            $weft_colour_5 = $this->input->post('weft_colour_5');
            $weft_colour_6 = $this->input->post('weft_colour_6');
            $weft_colour_7 = $this->input->post('weft_colour_7');
            $weft_colour_8 = $this->input->post('weft_colour_8');
            $weft_colour_9 = $this->input->post('weft_colour_9');
            $weft_colour_10 = $this->input->post('weft_colour_10');

            $status = $this->input->post('status');

            $data = array(
                'order_name' => isset($order_name) ? $order_name : '',
                'company_id' => isset($company_id) ? $company_id : '',
                'order_qty' => isset($order_qty) ? $order_qty : '',
                'reed_pick' => isset($reed_pick) ? $reed_pick : '',
                'cost' => isset($cost) ? $cost : '',
                'warp_count' => isset($warp_count) ? $warp_count : '',
                'warp_colours' => isset($warp_colours) ? $warp_colours : '',

                'warp_colour_1' => isset($warp_colour_1) ? $warp_colour_1 : '',
                'warp_colour_2' => isset($warp_colour_2) ? $warp_colour_2 : '',
                'warp_colour_3' => isset($warp_colour_3) ? $warp_colour_3 : '',
                'warp_colour_4' => isset($warp_colour_4) ? $warp_colour_4 : '',
                'warp_colour_5' => isset($warp_colour_5) ? $warp_colour_5 : '',
                'warp_colour_6' => isset($warp_colour_6) ? $warp_colour_6 : '',
                'warp_colour_7' => isset($warp_colour_7) ? $warp_colour_7 : '',
                'warp_colour_8' => isset($warp_colour_8) ? $warp_colour_8 : '',
                'warp_colour_9' => isset($warp_colour_9) ? $warp_colour_9 : '',
                'warp_colour_10' => isset($warp_colour_10) ? $warp_colour_10 : '',

                'loom_cost' => isset($loom_cost) ? $loom_cost : '',
                'warp_cost' => isset($warp_cost) ? $warp_cost : '',
                'knot_cost' => isset($knot_cost) ? $knot_cost : '',
                'hook_cost' => isset($hook_cost) ? $hook_cost : '',
                'pin_cost' => isset($pin_cost) ? $pin_cost : '',
                'weft_count' => isset($weft_count) ? $weft_count : '',
                'weft_colours' => isset($weft_colours) ? $weft_colours : '',

                'weft_colour_1' => isset($weft_colour_1) ? $weft_colour_1 : '',
                'weft_colour_2' => isset($weft_colour_2) ? $weft_colour_2 : '',
                'weft_colour_3' => isset($weft_colour_3) ? $weft_colour_3 : '',
                'weft_colour_4' => isset($weft_colour_4) ? $weft_colour_4 : '',
                'weft_colour_5' => isset($weft_colour_5) ? $weft_colour_5 : '',
                'weft_colour_6' => isset($weft_colour_6) ? $weft_colour_6 : '',
                'weft_colour_7' => isset($weft_colour_7) ? $weft_colour_7 : '',
                'weft_colour_8' => isset($weft_colour_8) ? $weft_colour_8 : '',
                'weft_colour_9' => isset($weft_colour_9) ? $weft_colour_9 : '',
                'weft_colour_10' => isset($weft_colour_10) ? $weft_colour_10 : '',

                'status' => $status,
                'order_image' => $order_image,
                'created_at' => date("m/d/y h:i:s")
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('order_data', $data);
            $this->session->set_flashdata('message', $this->upload->display_errors() . ' Order updated Successfully..');
            redirect('orders/view');


        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('order_data');
            $this->session->set_flashdata('message', 'Order deleted Successfully..');
            redirect('orders/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}