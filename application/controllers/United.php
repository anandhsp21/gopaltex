<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class United extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('UnitedEntry_model', 'united');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function imageUpload()
    {
        /*header('Access-Control-Allow-Origin: *');
        $target_path = "uploads/image_picker/";
        $target_path = $target_path . basename($_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            $data['success'] = "Upload and move success";
            $data['path'] = $target_path;
        } else {
            $data['path'] = $target_path;
            $data['error'] = "There was an error uploading the file, please try again!";
        }*/

        /*header('Access-Control-Allow-Origin: *');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            $data = array();
            $config = array(
                'upload_path' => './uploads/' . $folderPath,
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $data['error'] = $error;
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];

                if ($image_name) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'qty' => 0,
                        'name' => '',
                        'price' => 0,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $insert = $this->united->create($array_data, $folderPath);
                    if ($insert) {
                        $data['success'] = "Image uploaded successfully";
                    } else {
                        $data['error'] = "Image not uploaded";
                    }
                } else {
                    $data['error'] = "Image not uploaded yet..";
                }
            }
        } else
            {
            $data['error'] = 'folder path not defined';
        }*/
        $data['error'] = 'Kindly pay to enable the access';
        echo json_encode($data);
    }

    public function imageList()
    {
       /* header('Access-Control-Allow-Origin: *');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            $data['success'] = $this->united->allrecord($folderPath);
        } else {
            $data['error'] = 'image path not defined';
        }*/
        $data['error'] = 'Kindly pay to enable the access';
        echo json_encode($data);
    }

    public function imageEditList()
    {
       /* header('Access-Control-Allow-Origin: *');
        $name = $this->input->post('name');
        $price = $this->input->post('price');
        $qty = $this->input->post('qty');
        $id = $this->input->post('id');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            if ($name && $price && $id) {
                $updatedData = array(
                    'name' => isset($name) ? $name : '',
                    'price' => isset($price) ? $price : '',
                    'qty' => isset($qty) ? $qty : '',
                    'created_at' => date("m/d/y h:i:s")
                );
                $this->db->where('id', $id);
                $this->db->update('united_' . $folderPath, $updatedData);
                $data['success'] = 'Record updated successfully';

            } else {
                $data['error'] = 'values required';
            }
        } else {
            $data['error'] = 'image path not defined';
        }*/
        $data['error'] = 'Kindly pay to enable the access';
        echo json_encode($data);
    }

    public function imageDeleteList()
    {
        /*header('Access-Control-Allow-Origin: *');
        $id = $this->input->post('id');
        $image_name = $this->input->post('image_name');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            if ($id) {
                unlink("uploads/" . $folderPath . "/" . $image_name);
                $this->db->where('id', $id);
                $this->db->delete('united_' . $folderPath);
                $data['success'] = 'Record deleted successfully';
            } else {
                $data['error'] = 'values required';
            }
        } else {
            $data['error'] = 'image path not defined';
        }*/
        $data['error'] = 'Kindly pay to enable the access';
        echo json_encode($data);
    }

}