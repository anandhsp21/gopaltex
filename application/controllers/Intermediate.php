<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intermediate extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('IntermediatePayment_model', 'intermediate');
        $this->load->model('Employees_model', 'employee');
        $this->load->model('Orders_model', 'order');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('intermediate_payment/intermediate_payment_create');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function insert()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $date = $this->input->post('date');
            $desig = $this->input->post('desig');
            $emp_id = $this->input->post('emp_id');
            $amount = $this->input->post('amount');
            $purpose = $this->input->post('purpose');
            $status = $this->input->post('status');
            $data = array(
                'date' => isset($date) ? $date : '',
                'emp_id' => isset($emp_id) ? $emp_id : '',
                'desig' => isset($desig) ? $desig : '',
                'amount' => isset($amount) ? $amount : '',
                'purpose' => isset($purpose) ? $purpose : '',
                'status' => isset($status) ? $status : '',
                'created_at' => date("m/d/y h:i:s")
            );
            if ($data) {
                $insert = $this->intermediate->create($data);
                if ($insert) {
                    $this->session->set_flashdata('message', 'Intermediate Payment Created Successfully');
                    redirect('intermediate/view');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->CommonViewFn();
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }


    public function CommonViewFn()
    {
        if ($this->input->post('date') != "") {
            $search_date = trim($this->input->post('date'));
        } else {
            $search_date = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
        }
        $data['search_date'] = $search_date;
        $allrecord = $this->intermediate->allrecord($search_date);
        $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $search_date;
        $paging = array();
        $paging['base_url'] = $baseurl;
        $paging['total_rows'] = $allrecord;
        $paging['query_string_segment'] = 'per_page';
        $paging['per_page'] = 10;
        $paging['uri_segment'] = 4;
        $paging['num_links'] = 5;
        $paging['first_link'] = 'First';
        $paging['first_tag_open'] = '<li>';
        $paging['first_tag_close'] = '</li>';
        $paging['num_tag_open'] = '<li>';
        $paging['num_tag_close'] = '</li>';
        $paging['prev_link'] = 'Prev';
        $paging['prev_tag_open'] = '<li>';
        $paging['prev_tag_close'] = '</li>';
        $paging['next_link'] = 'Next';
        $paging['next_tag_open'] = '<li>';
        $paging['next_tag_close'] = '</li>';
        $paging['last_link'] = 'Last';
        $paging['last_tag_open'] = '<li>';
        $paging['last_tag_close'] = '</li>';
        $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $paging['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($paging);
        $data['limit'] = $paging['per_page'];
        $data['number_page'] = $paging['per_page'];
        $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
        $data['nav'] = $this->pagination->create_links();
        $data['datas'] = $this->intermediate->data_list($data['limit'], $data['offset'], $search_date);
        $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('','');
        $data['orders_ids'] = $this->order->GetAllOrderRecords('');

        $this->load->view('template/header');
        $this->load->view('template/home_login_section');
        $this->load->view('intermediate_payment/intermediate_payment_view', $data, FALSE);
        $this->load->view('template/footer');

    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->intermediate->entry_edit_data($id);
            $data['edit_data'] = $result_data[0];
            $data['employee_ids'] = $this->employee->GetAllEmployeeRecords('','');
            $this->load->view('intermediate_payment/intermediate_payment_edit', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $date = $this->input->post('date');
            $desig = $this->input->post('desig');
            $emp_id = $this->input->post('emp_id');
            $amount = $this->input->post('amount');
            $purpose = $this->input->post('purpose');
            $status = $this->input->post('status');
            $data = array(
                'date' => isset($date) ? $date : '',
                'emp_id' => isset($emp_id) ? $emp_id : '',
                'desig' => isset($desig) ? $desig : '',
                'amount' => isset($amount) ? $amount : '',
                'purpose' => isset($purpose) ? $purpose : '',
                'status' => isset($status) ? $status : '',
                'created_at' => date("m/d/y h:i:s")
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('intermediate_payment_data', $data);
            $this->session->set_flashdata('message', 'Intermediate Payment Updated Successfully..');
            redirect('intermediate/view');

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('intermediate_payment_data');
            $this->session->set_flashdata('message', 'Intermediate Payment deleted Successfully..');
            redirect('intermediate/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}