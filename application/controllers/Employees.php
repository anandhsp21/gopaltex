<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Employees_model', 'employee');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('employee/employee_create');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = array(
                'upload_path' => './uploads/employee',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('userfile')) {
                $upload_data = $this->upload->data();
                $emp_image = $upload_data['file_name'];
            } else {
                $emp_image = '';
            }
            $data = array(
                'emp_name' => $this->input->post('emp_name'),
                'father_name' => $this->input->post('father_name'),
                'age' => $this->input->post('age'),
                'desig' => $this->input->post('desig'),
                'mobile' => $this->input->post('mobile'),
                'doj' => $this->input->post('doj'),
                'advance' => $this->input->post('advance'),
                'account_no' => $this->input->post('account_no'),
                'ifsc' => $this->input->post('ifsc'),
                'aadhar_id' => $this->input->post('aadhar_id'),
                'balance' => $this->input->post('balance'),
                'status' => $this->input->post('status'),
                'employee_image' => $emp_image,
                'created_at' => date("m/d/y h:i:s")
            );
            if ($data) {
                $insert = $this->employee->create($data);
                if ($insert) {
                    $this->session->set_flashdata('message', $this->upload->display_errors() . ' Employee Created Successfully');
                    redirect('employees/view');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            if ($this->input->post('emp_name') != "") {
                $emp_name = trim($this->input->post('emp_name'));
            } else {
                $emp_name = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            }
            $data['search_title'] = $emp_name;
            $allrecord = $this->employee->allrecord($emp_name);
            $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $emp_name;
            $paging = array();
            $paging['base_url'] = $baseurl;
            $paging['total_rows'] = $allrecord;
            $paging['query_string_segment'] = 'per_page';
            $paging['per_page'] = 10;
            $paging['uri_segment'] = 4;
            $paging['num_links'] = 5;
            $paging['first_link'] = 'First';
            $paging['first_tag_open'] = '<li>';
            $paging['first_tag_close'] = '</li>';
            $paging['num_tag_open'] = '<li>';
            $paging['num_tag_close'] = '</li>';
            $paging['prev_link'] = 'Prev';
            $paging['prev_tag_open'] = '<li>';
            $paging['prev_tag_close'] = '</li>';
            $paging['next_link'] = 'Next';
            $paging['next_tag_open'] = '<li>';
            $paging['next_tag_close'] = '</li>';
            $paging['last_link'] = 'Last';
            $paging['last_tag_open'] = '<li>';
            $paging['last_tag_close'] = '</li>';
            $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $paging['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($paging);
            $data['limit'] = $paging['per_page'];
            $data['number_page'] = $paging['per_page'];
            $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
            $data['nav'] = $this->pagination->create_links();
            $data['datas'] = $this->employee->data_list($data['limit'], $data['offset'], $emp_name);

            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('employee/employee_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }


    public function report()
    {
        $data['datas'] = $this->employee->GetAllEmployeePdfRecords();
        $data['title'] = 'Employee Report';
        $html = $this->load->view('employee/employee_pdf_view', $data, TRUE);
        $pdf_name = 'Employee';
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdf_name . '.pdf', 'I');
    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->employee->employe_edit_data($id);
            $this->data['edit_data'] = $result_data[0];
            $this->load->view('employee/employee_edit', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $config = array(
                'upload_path' => './uploads/employee',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('userfile')) {
                $upload_data = $this->upload->data();
                $emp_image = $upload_data['file_name'];
            }else{
                $emp_image = $this->input->post('UserImageFile');
            }
            $data = array(
                'emp_name' => $this->input->post('emp_name'),
                'father_name' => $this->input->post('father_name'),
                'age' => $this->input->post('age'),
                'desig' => $this->input->post('desig'),
                'mobile' => $this->input->post('mobile'),
                'doj' => $this->input->post('doj'),
                'advance' => $this->input->post('advance'),
                'account_no' => $this->input->post('account_no'),
                'ifsc' => $this->input->post('ifsc'),
                'aadhar_id' => $this->input->post('aadhar_id'),
                'balance' => $this->input->post('balance'),
                'status' => $this->input->post('status'),
                'employee_image' => $emp_image,
                'created_at' => date("m/d/y h:i:s")
            );
            if ($data) {
                $this->db->where('id', $this->input->post('id'));
                $this->db->update('employee_data', $data);
                $this->session->set_flashdata('message', 'Employee Updated Successfully');
                redirect('employees/view');
            }

        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('employee_data');
            $this->session->set_flashdata('message', 'Employee deleted Successfully..');
            redirect('employees/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}