<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Company_model', 'company');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('company/company_create');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function create()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $data = array(
                'company_name' => $this->input->post('company_name'),
                'contact_name' => $this->input->post('contact_name'),
                'contact_num_1' => $this->input->post('contact_num_1'),
                'contact_num_2' => $this->input->post('contact_num_2'),
                'address' => $this->input->post('address'),
                'gst' => $this->input->post('gst'),
                'status' => $this->input->post('status'),
                'created_at' => date("m/d/y h:i:s")
            );
            $insert = $this->company->create($data);
            if ($insert) {
                $this->session->set_flashdata('message', 'Company Created Successfully');
                redirect('company/view');
            }
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            if ($this->input->post('company_name') != "") {
                $company_name = trim($this->input->post('company_name'));
            } else {
                $company_name = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            }
            $data['search_title'] = $company_name;
            $allrecord = $this->company->allrecord($company_name);
//          var_dump($allrecord);die;
            $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $company_name;
            $paging = array();
            $paging['base_url'] = $baseurl;
            $paging['total_rows'] = $allrecord;
            $paging['query_string_segment'] = 'per_page';
            $paging['per_page'] = 10;
            $paging['uri_segment'] = 4;
            $paging['num_links'] = 5;
            $paging['first_link'] = 'First';
            $paging['first_tag_open'] = '<li>';
            $paging['first_tag_close'] = '</li>';
            $paging['num_tag_open'] = '<li>';
            $paging['num_tag_close'] = '</li>';
            $paging['prev_link'] = 'Prev';
            $paging['prev_tag_open'] = '<li>';
            $paging['prev_tag_close'] = '</li>';
            $paging['next_link'] = 'Next';
            $paging['next_tag_open'] = '<li>';
            $paging['next_tag_close'] = '</li>';
            $paging['last_link'] = 'Last';
            $paging['last_tag_open'] = '<li>';
            $paging['last_tag_close'] = '</li>';
            $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $paging['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($paging);
            $data['limit'] = $paging['per_page'];
            $data['number_page'] = $paging['per_page'];
            $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
            $data['nav'] = $this->pagination->create_links();
            $data['datas'] = $this->company->data_list($data['limit'], $data['offset'], $company_name);

            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $this->load->view('company/company_view', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function report()
    {
        $data['datas'] = $this->company->GetAllCompanyPdfRecords();
        $data['title'] = 'Company Report';
        $html = $this->load->view('company/company_pdf_view', $data, TRUE);
        $pdf_name = 'company';
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdf_name . '.pdf', 'I');
    }

    public function edit($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/home_login_section');
            $result_data = $this->company->employe_edit_data($id);
            $this->data['edit_data'] = $result_data[0];
            $this->load->view('company/company_edit', $this->data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function update()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $data = array(
                'company_name' => $this->input->post('company_name'),
                'contact_name' => $this->input->post('contact_name'),
                'contact_num_1' => $this->input->post('contact_num_1'),
                'contact_num_2' => $this->input->post('contact_num_2'),
                'address' => $this->input->post('address'),
                'gst' => $this->input->post('gst'),
                'status' => $this->input->post('status'),
                'created_at' => date("m/d/y h:i:s")
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('company_data', $data);
            $this->session->set_flashdata('message', 'Company updated Successfully..');
            redirect('company/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function delete($id)
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('company_data');
            $this->session->set_flashdata('message', 'Company deleted Successfully..');
            redirect('company/view');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}