<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Users_model', 'users');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $this->view();
    }

    public function login_data()
    {
        $data = array(
            'username' => trim($this->input->post('username')),
            'password' => md5(trim($this->input->post('password')))
        );
        $get_data = $this->users->checkusername($data);
        if (isset($get_data) && is_array($get_data) && !empty($get_data)) {
            $user_session = array(
                'id' => $get_data[0]['id'],
                'firstname' => $get_data[0]['firstname'],
                'lastname' => $get_data[0]['lastname'],
                'username' => $get_data[0]['username'],
                'email' => $get_data[0]['email'],
                'role_id' => $get_data[0]['role_id'],
                'sex' => $get_data[0]['sex'],
                'address' => $get_data[0]['address']
            );
            $this->session->set_userdata($user_session);
            $this->session->set_flashdata('message', 'You have been successfully logged ..');
            redirect('home/view');
        } else {
            $this->session->set_flashdata('message', 'Invalid Login details');
            redirect('login');
        }
    }

    public function view()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $data['active'] = 'active';
            $data['count'] = $this->users->count_of_records_from_tables();
            $this->load->view('template/home_login_section', $data, FALSE);
            $this->load->view('home/home_view');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }
}