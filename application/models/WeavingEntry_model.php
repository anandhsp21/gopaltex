<?php

class WeavingEntry_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('weaving_entry_data', $data);
        return TRUE;
    }

    function insert_all($dataSet)
    {
        $this->db->insert_batch('weaving_entry_data', $dataSet);
        return $this->db->insert_id(); // this will return the id of last item inserted.
    }

    public function allrecord($name)
    {
        $this->db->select('*');
        $this->db->from('weaving_entry_data');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
//        $this->db->where('status', 'active');
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        $this->db->select('*');
        $this->db->from('weaving_entry_data');
        $this->db->order_by('id', 'desc');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
//        $this->db->where('status', 'active');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function entry_edit_data($id){
        $query = $this->db->query("SELECT * FROM weaving_entry_data WHERE id = $id");
        return $query->result_array();
    }

}