<?php

class WindingEntry_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('winding_entry_data', $data);
        return TRUE;
    }

    public function allrecord($name)
    {
        $this->db->select('*');
        $this->db->from('winding_entry_data');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
//        $this->db->where('status', 'active');
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        $this->db->select('*');
        $this->db->from('winding_entry_data');
        $this->db->order_by('id', 'desc');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
//        $this->db->where('status', 'active');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function entry_edit_data($id){
        $query = $this->db->query("SELECT * FROM winding_entry_data WHERE id = $id");
        return $query->result_array();
    }

}