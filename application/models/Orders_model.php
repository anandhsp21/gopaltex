<?php

class Orders_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('order_data', $data);
        return TRUE;
    }

    public function allrecord($name)
    {
        if (!empty($name)) {
            $this->db->like('order_name', $name);
        }
        $this->db->select('*');
        $this->db->from('order_data');
//        $this->db->where('status', 'active');
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        if ($name) {
            $this->db->like('order_name', $name);
        }
        $this->db->select('*');
        $this->db->from('order_data');
        $this->db->order_by('id', 'desc');
//        $this->db->where('status', 'active');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function order_edit_data($id)
    {
        $query = $this->db->query("SELECT * FROM order_data WHERE id = $id");
        return $query->result_array();
    }

    public function GetAllOrderRecords($status)
    {
        if ($status) {
            $sql = "WHERE status = 'active'";
        } else {
            $sql = '';
        }
        $query = $this->db->query("SELECT * FROM order_data $sql ORDER BY id");
        return $query->result_array();
    }

    public function GetAllOrderRecordsByCompanyId($company_id, $status)
    {
        if ($company_id && $status) {
            $sql = "WHERE status = 'active' AND company_id = '$company_id'";
        } else if ($company_id) {
            $sql = "WHERE company_id = '$company_id'";
        } else {
            $sql = '';
        }
        $query = $this->db->query("SELECT * FROM order_data $sql ORDER BY id");
        return $query->result_array();
    }

    public function GetAllOrderPdfRecords()
    {
        $query = $this->db->query("SELECT * FROM order_data ORDER BY id ASC");
        return $query->result_array();
    }

    public function GetRecordByOrderIdNumberRows($id, $search_date)
    {
        if ($search_date) {
            $sql = "AND weaving_entry_data.date='$search_date'";
        } else {
            $sql = '';
        }
        $query = $this->db->query("SELECT weaving_entry_data.id as id, emp_name, order_data.status as order_status, order_qty, company_name, meters, order_name, loom_no, date FROM weaving_entry_data 
LEFT JOIN order_data ON weaving_entry_data.order_id = order_data.id
LEFT JOIN employee_data ON employee_data.id = weaving_entry_data.emp_id 
LEFT JOIN company_data ON order_data.company_id = company_data.id 
WHERE weaving_entry_data.order_id = $id  $sql ORDER BY weaving_entry_data.id DESC");
        return $query->num_rows();
    }

    public function GetRecordByOrderId($id)
    {
        $query = $this->db->query("SELECT weaving_entry_data.id as id, emp_name, order_data.status as order_status, order_qty, company_name, meters, order_name, loom_no, date FROM weaving_entry_data 
LEFT JOIN order_data ON weaving_entry_data.order_id = order_data.id
LEFT JOIN employee_data ON employee_data.id = weaving_entry_data.emp_id 
LEFT JOIN company_data ON order_data.company_id = company_data.id 
WHERE (weaving_entry_data.order_id = $id) ORDER BY weaving_entry_data.id DESC");
        return $query->result_array();
    }

    public function pdf_data_list($limit, $offset, $id, $search_date)
    {
        if ($search_date) {
            $sql = "AND weaving_entry_data.date='$search_date'";
        } else {
            $sql = '';
        }
        $query = $this->db->query("SELECT weaving_entry_data.id as id, emp_name, order_data.status as order_status, order_qty, company_name, meters, order_name, loom_no, date FROM weaving_entry_data 
LEFT JOIN order_data ON weaving_entry_data.order_id = order_data.id
LEFT JOIN employee_data ON employee_data.id = weaving_entry_data.emp_id 
LEFT JOIN company_data ON order_data.company_id = company_data.id 
WHERE weaving_entry_data.order_id = $id $sql ORDER BY weaving_entry_data.id DESC LIMIT $limit OFFSET $offset");
        return $query->result_array();
    }

    public function get_com_order_name_by_order_id($id)
    {
        $query = $this->db->query("SELECT company_name, order_name FROM weaving_entry_data 
LEFT JOIN order_data ON weaving_entry_data.order_id = order_data.id
LEFT JOIN company_data ON order_data.company_id = company_data.id 
WHERE weaving_entry_data.order_id = $id LIMIT 1");
        return $query->result_array();
    }
}