<?php

class UnitedEntry_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data, $foldername)
    {
        $this->db->insert('united_' . $foldername, $data);
        return TRUE;
    }

    public function allrecord($foldername)
    {
        $this->db->select('*');
        $this->db->from('united_' . $foldername);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function data_list($limit, $offset, $name)
    {
        $this->db->select('*');
        $this->db->from('warping_entry_data');
        $this->db->order_by('id', 'desc');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
//        $this->db->where('status', 'active');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function entry_edit_data($id)
    {
        $query = $this->db->query("SELECT * FROM warping_entry_data WHERE id = $id");
        return $query->result_array();
    }

}