<?php

class Wage_Payment_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('pin_entry_data', $data);
        return TRUE;
    }

    public function weekly_payment_data_create($data)
    {
        $this->db->insert('weekly_payment_data', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function pdf_data_create($data)
    {
        $this->db->insert('pdf_rollback_data', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getEmpIdFromPdfTable($id)
    {
        $query = $this->db->query("SELECT * FROM pdf_rollback_data WHERE emp_id = $id");
        return $query->result_array();
    }

    public function getDataFromPdfTable($id)
    {
        $query = $this->db->query("SELECT * FROM pdf_rollback_data WHERE id = $id");
        return $query->result_array();
    }

    public function allrecord($name)
    {
        $this->db->select('*');
        $this->db->from('pin_entry_data');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function pdfallrecord($name, $emp_id)
    {
        $this->db->select('*');
        $this->db->from('pdf_rollback_data');
        if ($name) {
            $this->db->like('date', $name);
        }
        if ($emp_id) {
            $this->db->like('emp_id', $emp_id);
        }
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        $this->db->select('*');
        $this->db->from('pin_entry_data');
        $this->db->order_by('id', 'desc');
        if (!empty($name)) {
            $this->db->like('date', $name);
        }
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function pdf_data_list($limit, $offset, $name, $emp_id)
    {
        $this->db->select('*');
        $this->db->from('pdf_rollback_data');
        $this->db->order_by('id', 'desc');
        if ($name) {
            $this->db->like('date', $name);
        }
        if ($emp_id) {
            $this->db->like('emp_id', $emp_id);
        }
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function entry_edit_data($id)
    {
        $query = $this->db->query("SELECT * FROM pin_entry_data WHERE id = $id");
        return $query->result_array();
    }

    public function getEmpNameFromId($id)
    {
        $query = $this->db->query("SELECT emp_name FROM employee_data WHERE id = $id");
        return $query->result_array();
    }

    public function getInterValueFromId($id)
    {
        $query = $this->db->query("SELECT id,amount FROM intermediate_payment_data WHERE emp_id = $id AND status = 'not_tallied'");
        return $query->result_array();
    }

    public function getPrevBalanceFromId($id)
    {
        $query = $this->db->query("SELECT id, balance FROM employee_data WHERE id = $id AND status = 'active'");
        return $query->result_array();
    }

    public function calculate_wage($data, $db_name)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $emp_id = $data['emp_id'];
        $payment_status = $data['payment_status'];
        if ($from_date & $to_date) {
                 $sql = "SELECT * FROM $db_name WHERE payment_status = '$payment_status' AND emp_id = '$emp_id' AND STR_TO_DATE(date, '%d-%m-%Y') BETWEEN STR_TO_DATE('$from_date', '%d-%m-%Y')
                   AND STR_TO_DATE('$to_date', '%d-%m-%Y')";
        } else {
            if ($from_date) {
                $selected_date = $from_date;
            } else {
                $selected_date = $to_date;
            }
            $sql = "SELECT * FROM $db_name WHERE payment_status = '$payment_status' AND date = '$selected_date' AND emp_id = '$emp_id'";
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}