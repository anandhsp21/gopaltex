<?php

class Users_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('user_data', $data);
        return TRUE;
    }

    public function allrecord($name)
    {
        if ($name) {
            $this->db->like('username', $name);
        }
        $this->db->select('*');
        $this->db->from('user_data');
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        if (!empty($name)) {
            $this->db->like('company_name', $name);
        }
        $this->db->select('*');
        $this->db->from('user_data');
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function user_edit_data($id){
        $query = $this->db->query("SELECT * FROM user_data WHERE id = $id");
        return $query->result_array();
    }

    public function count_of_records_from_tables()
    {
        $query = $this->db->query("select 
(select count(*) from company_data) as company_data_count,
(select count(*) from order_data) as order_data_count,
(select count(*) from employee_data) as employee_data_count, 
(SELECT COUNT(*) FROM hook_entry_data as hook_entry_data) +
(SELECT COUNT(*) FROM knotting_entry_data as knotting_entry_data_data) +
(SELECT COUNT(*) FROM pin_entry_data as pin_entry_data_data) +
(SELECT COUNT(*) FROM warping_entry_data as warping_entry_data_data) +
(SELECT COUNT(*) FROM weaving_entry_data as weaving_entry_data_data) +
(SELECT COUNT(*) FROM winding_entry_data as winding_entry_data_data)
as entry_data");
        return $query->row_array();
    }

    public function view_data()
    {
        $query = $this->db->query("SELECT * FROM user_data WHERE status = 'Y' ORDER BY id ASC");
        return $query->result_array();
    }

    public function users_view_data($id)
    {
        $query = $this->db->query("SELECT * FROM users_data WHERE user_id = $id ORDER BY id ASC");
        return $query->result_array();
    }

    public function user_view_data($id)
    {
        $query = $this->db->query("SELECT * FROM user_data WHERE id = $id ORDER BY id ASC");
        return $query->result_array();
    }

    public function edit_data($id)
    {
        $query = $this->db->query("SELECT * FROM user_data WHERE id = $id");
        return $query->result_array();
    }

    public function checkusername($data)
    {
        $query = "SELECT * FROM user_data WHERE username = '" . $data['username'] . "' and pass = '" . $data['password'] . "'";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function getusername($id)
    {
        $query = "SELECT user_data.id as id,user_data.username as user_name,users_data.document_name as image,comments_data.comment as comment_data,comments_data.document_id as document_id,comments_data.role_id as role_id FROM user_data
                    LEFT JOIN users_data ON user_data.id = users_data.user_id
                    LEFT JOIN comments_data ON users_data.id = comments_data.document_id
                    WHERE comments_data.id = $id";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function getallusername($id)
    {
        $query = "SELECT user_data.id as id,user_data.username as user_name,users_data.document_name as image,comments_data.comment as comment_data,comments_data.document_id as document_id,comments_data.role_id as role_id FROM user_data
                    LEFT JOIN users_data ON user_data.id = users_data.user_id
                    LEFT JOIN comments_data ON users_data.id = comments_data.document_id
                    WHERE comments_data.document_id = $id";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function comment_count($id)
    {
        $query = "SELECT COUNT(document_id) FROM comments_data WHERE document_id =" . $id;
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function likes_count_total($id)
    {
        $status = "'Y'";
        $query = "SELECT COUNT(likes_count) as count, status FROM `likes_data` WHERE `document_id` = " . $id . " AND `status` = $status";
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function comment_insert_data($data)
    {
        $this->db->insert('comments_data', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function likes_insert_data($data)
    {
        $query = $this->db->insert('likes_data', $data);
        return TRUE;
    }

    public function checking_inserted_data($data)
    {
        $query = "SELECT * FROM `likes_data` WHERE `document_id` = " . $data['document_id'] . " AND `user_id` = " . $data['user_id'] . " AND `role_id` = " . $data['role_id'] . "";
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function getting_total_count_likes($id)
    {
        $status = "'Y'";
        $query = "SELECT COUNT(likes_count) as count, status FROM `likes_data` WHERE `document_id` = " . $id . " AND `status` = $status";
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function updating_likes_insert_data($values, $likes_count, $likes_data_id, $status)
    {
        $data = array('likes_count' => $likes_count, 'status' => $status);
        $this->db->where('id', $likes_data_id);
        $query = $this->db->update('likes_data', $data);
        return TRUE;
    }


}