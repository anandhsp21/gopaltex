<?php

class Company_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('company_data', $data);
        return TRUE;
    }

    public function allrecord($name)
    {
        if (!empty($name)) {
            $this->db->like('company_name', $name);
        }
        $this->db->select('*');
        $this->db->from('company_data');
//        $this->db->where('status', 'active');
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        if (!empty($name)) {
            $this->db->like('company_name', $name);
        }
        $this->db->select('*');
        $this->db->from('company_data');
        $this->db->order_by('id', 'desc');
//      $this->db->where('status', 'active');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function employe_edit_data($id){
        $query = $this->db->query("SELECT * FROM company_data WHERE id = $id");
        return $query->result_array();
    }

    public function GetAllCompanyRecords(){
        $query = $this->db->query("SELECT id,company_name FROM company_data WHERE status = 'active' ORDER BY id");
        return $query->result_array();
    }
    public function GetAllCompanyPdfRecords(){
        $query = $this->db->query("SELECT * FROM company_data ORDER BY id ASC");
        return $query->result_array();
    }
}