<?php

class Employees_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data)
    {
        $this->db->insert('employee_data', $data);
        return TRUE;
    }

    public function allrecord($name)
    {
        if (!empty($name)) {
            $this->db->like('emp_name', $name);
        }
        $this->db->select('*');
        $this->db->from('employee_data');
//        $this->db->where('status', 'active');
        $rs = $this->db->get();
        return $rs->num_rows();
    }

    public function data_list($limit, $offset, $name)
    {
        if (!empty($name)) {
            $this->db->like('emp_name', $name);
        }
        $this->db->select('*');
        $this->db->from('employee_data');
        $this->db->order_by('id', 'desc');
//        $this->db->where('status', 'active');
        $this->db->limit($limit, $offset);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function employe_edit_data($id)
    {
        $query = $this->db->query("SELECT * FROM employee_data WHERE id = $id");
        return $query->result_array();
    }

    public function GetAllEmployeeRecords($desig_type, $status)
    {

        if ($desig_type && $status) {
            $sql = "WHERE status = 'active' AND desig = '$desig_type'";
        } else if ($desig_type) {
            $sql = "WHERE desig = '$desig_type'";
        } else {
            $sql = '';
        }
        $query = $this->db->query("SELECT id,emp_name FROM employee_data $sql ORDER BY id");
        return $query->result_array();
    }

    public function GetAllEmployeePdfRecords(){
        $query = $this->db->query("SELECT * FROM employee_data ORDER BY id ASC");
        return $query->result_array();
    }
}