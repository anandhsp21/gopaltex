<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Deliver in Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('deliver'); ?>">Deliver in Entry</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Deliver in Entry</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
            <form method="post" action="<?php echo site_url('deliverin/insert'); ?>" enctype="multipart/form-data">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">


                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="machine_no">Slip No:</label>
                        <input type="text" required placeholder="Slip No" class="form-control" name="slip_no"
                               id="slip_no">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="company_id">Company Name:</label>
                        <select class="selectpicker company_click form-control required" id="company_id" name="company_id">
                            <option value="">-- <?php if(!$all_company_data || empty($all_company_data)){ echo 'No'; } ?> Company Name--</option>
                            <?php
                            if (isset($all_company_data)) {
                                $i = 0;
                                foreach ($all_company_data as $data) { ?>
                                    <option value="<?php echo $data['id']; ?>"><?php echo $data['company_name'] ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_colours">Warp Colours:</label>
                        <select disabled required class="selectpicker form-control warp_colours" id="warp_colours" name="warp_colours">
                            <option value="">--Warp Colours--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <span class="warp_colour_open_close">

                    </span>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">


                    <div class="form-group padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="date">Date:</label>

                        <div class="input-group date doj" data-provide="datepicker">
                            <input type="text" name="date" id="date" class="form-control">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="order_name">Order Name:</label>
                        <select required class="selectpicker order_list_name form-control" id="order_name" name="order_name">
                            <option value=""> --No Order Name-- </option>
                            <?php
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="weft_colours">Weft Colours:</label>
                        <select disabled required class="selectpicker form-control weft_colours" id="weft_colours" name="weft_colours">
                            <option value="">--Weft Colours--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <span class="weft_colour_open_close">

                    </span>


                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                    <a href="<?php echo base_url(); ?>deliverin" class="btn btn-info" role="button">Back</a>
                    <button type="submit" class="btn btn-primary text-center">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
