<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Payment Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('wage'); ?>">Wage Payment</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('intermediate/view'); ?>">Intermediate Payment</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('supplier/view'); ?>">Supplier Payment</a>
                    </li>
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('payreport/view'); ?>">Payment Report</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('wage/invoice'); ?>">Paid invoice</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">View Invoice</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title margin_top_ten col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">
                <form method="post" action="<?php echo base_url(); ?>wage/invoice">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group search_button col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input-group date doj" data-provide="datepicker">
                                <input placeholder="search date" value="<?php if (!empty($search_date)) {
                                    echo $search_date;
                                } ?>" type="text" name="date" id="date" class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <select class="selectpicker form-control" id="emp_id" name="emp_id">
                                <option value="">-- <?php if (!$employee_ids || empty($employee_ids)) {
                                        echo 'No';
                                    } ?> Employee Name--
                                </option>
                                <?php
                                if (isset($employee_ids)) {
                                    foreach ($employee_ids as $data) { ?>
                                        <option <?php if ($selected_id == $data['id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $data['id'] ?>"><?php echo $data['emp_name'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center margin_top_ten">
                            <input type="submit" name="search" class="btn btn-primary">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center margin_top_ten">
                            <input type="button" value="Reset" class="reset_button btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom table_over_flow padding_left_null padding_right_null">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Bill No</th>
                        <th>Date</th>
                        <th>Emp Name</th>
                        <th>Status</th>
                        <th>View Bill</th>
                        <th>Cancel Bill</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (isset($datas) && is_array($datas) && count($datas)) {
                        $i = 0;
                        foreach ($datas as $data) {
                            ?>
                            <tr>
                                <td><?php echo $data["id"]; ?></td>
                                <td><?php echo $data["date"]; ?></td>
                                <td>
                                    <?php if (isset($employee_ids) && is_array($employee_ids) && count($employee_ids)) {
                                        foreach ($employee_ids as $employee_id) {
                                            if (intval($employee_id['id']) == intval($data['emp_id'])) {
                                                echo $employee_id["emp_name"];
                                            }
                                        }
                                    } ?>
                                </td>
                                <td><?php echo $data["status"]; ?></td>
                                <td><a href="<?php echo site_url('wage/view_pdf/' . $data['id'] . ''); ?>">View</a>
                                </td>
                                <td><a href="<?php echo site_url('wage/cancel_pdf/' . $data['id'] . ''); ?>">Cancel</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <?php if (!$datas) { ?>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Pdf Entries
                        found</p>
                <?php } ?>
            </div>
            <div class="pagination-dive col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $nav; ?>
            </div>
        </div>
    </div>
</div>
