<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Entry Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry'); ?>">Weaving Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('warping/view'); ?>">Warping Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('knotting/view'); ?>">Knotting Entry</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('pin/view'); ?>">Pin Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('hook/view'); ?>">Hook Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('winding/view'); ?>">Winding Entry</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Pin Entry</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
            <form method="post" action="<?php echo site_url('pin/insert'); ?>" enctype="multipart/form-data">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">

                    <div class="form-group padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="date">Date:</label>

                        <div class="input-group date doj" data-provide="datepicker">
                            <input type="text" name="date" id="date" class="form-control">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="emp_id">Emp Name:</label>
                        <select required class="selectpicker form-control" id="emp_id" name="emp_id">
                            <option value="">-- <?php if (!$all_employee_data || empty($all_employee_data)) {
                                    echo 'No';
                                } ?> Employee Name--
                            </option>
                            <?php
                            if (isset($all_employee_data)) {
                                $i = 0;
                                foreach ($all_employee_data as $data) { ?>
                                    <option value="<?php echo $data['id'] ?>"><?php echo $data['emp_name'] ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="machine_no">loom No:</label>
                        <input type="text" required placeholder="Loom No" class="form-control" name="loom_no"
                               id="loom_no">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="order_name">Order Name:</label>
                        <select required class="selectpicker form-control" id="order_name" name="order_id">
                            <option value="">-- <?php if (!$all_order_data || empty($all_order_data)) {
                                    echo 'No';
                                } ?> Order Name--
                            </option>
                            <?php
                            if (isset($all_order_data)) {
                                $i = 0;
                                foreach ($all_order_data as $data) { ?>
                                    <option value="<?php echo $data['id'] ?>"><?php echo $data['order_name'] ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                    </div>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                    <a href="<?php echo base_url(); ?>pin" class="btn btn-info" role="button">Back</a>
                    <button type="submit" class="btn btn-primary text-center">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
