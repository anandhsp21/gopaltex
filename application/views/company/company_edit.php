<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Employee Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light">
                <ul class="navbar-nav">
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company'); ?>">Add Company</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company/view'); ?>">View Company</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company/report'); ?>">Company Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Edit Employee</span></p>
        </div>
        <?php if (isset($edit_data) && is_array($edit_data) && count($edit_data)){ ?>
        <form method="post" action="<?php echo site_url('company/update/'); ?>" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>"/>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="emp_name">Company Name:</label>
                        <input type="text" required placeholder="Company Name"
                               value="<?php echo $edit_data['company_name']; ?>" class="form-control" name="company_name"
                               id="company_name">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="father_name">Contact Name:</label>
                        <input type="text" required placeholder="Contact Name"
                               value="<?php echo $edit_data['contact_name']; ?>" class="form-control" name="contact_name"
                               id="contact_name">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="age">Contact No:1</label>
                        <input type="text" required placeholder="Contact No:1" value="<?php echo $edit_data['contact_num_1']; ?>"
                               class="form-control" name="contact_num_1" id="contact_num_1">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="mobile">Contact No:2</label>
                        <input type="text" required placeholder="Contact No:2" value="<?php echo $edit_data['contact_num_2']; ?>"
                               class="form-control" name="contact_num_2"
                               id="contact_num_2">
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="advance">Address:</label>
                        <input type="text" required placeholder="Address" value="<?php echo $edit_data['address']; ?>"
                               class="form-control" name="address"
                               id="address">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="account_no">GST no:</label>
                        <input type="text" required placeholder="GST no"
                               value="<?php echo $edit_data['gst']; ?>" class="form-control" name="gst"
                               id="gst">
                    </div>
                    <div class="form-group padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="status">Status:</label>
                        <select required class="selectpicker form-control" id="status" name="status">
                            <option value="">--Status--</option>
                            <option <?php if ($edit_data['status'] == 'active') {
                                echo 'selected';
                            } ?> value="active">Active
                            </option>
                            <option <?php if ($edit_data['status'] == 'inactive') {
                                echo 'selected';
                            } ?> value="inactive">In Active
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>company/view" class="btn btn-info" role="button">Back</a>
                <button type="submit" class="btn btn-primary text-center">Submit</button>
            </div>
            <?php } ?>
        </form>
    </div>
</div>
