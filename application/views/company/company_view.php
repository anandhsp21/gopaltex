<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Employee Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company'); ?>">Add Company</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company/view'); ?>">View Company</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company/report'); ?>">Company Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12 padding_left_null padding_right_null">
            <p><span class="wow fadeInDown animated">View Employee</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title margin_top_ten col-xs-12 padding_left_null padding_right_null">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">
                <form method="post" action="<?php echo base_url(); ?>company/view">
                    <div class="form-group search_button col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 reset_empty">
                            <input type="text" placeholder="Company Name" class="form-control" id="title"
                                   name="company_name"
                                   value="<?php if (!empty($search_title)) {
                                       echo $search_title;
                                   } ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input type="submit" name="search" class="btn btn-primary">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input type="button" value="Reset" class="reset_button btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom table_over_flow padding_left_null padding_right_null">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Company Name</th>
                        <th>Contact Name</th>
                        <th>Contact No1</th>
                        <th>Contact No2</th>
                        <th>Address</th>
                        <th>Gst</th>
                        <th>Status</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (isset($datas) && is_array($datas) && count($datas)) {
                        $i = 0;
                        foreach ($datas as $data) {
                            ?>
                            <tr>
                                <td><?php echo $data["id"]; ?></td>
                                <td><?php echo $data["company_name"]; ?></td>
                                <td><?php echo $data["contact_name"]; ?></td>
                                <td><?php echo $data["contact_num_1"]; ?></td>
                                <td><?php echo $data["contact_num_2"]; ?></td>
                                <td><?php echo $data["address"]; ?></td>
                                <td><?php echo $data["gst"]; ?></td>
                                <td><?php echo $data["status"]; ?></td>
                                <td><a href="<?php echo site_url('company/edit/' . $data['id'] . ''); ?>">View</a>
                                </td>
                                <td><a href="<?php echo site_url('company/delete/' . $data['id'] . ''); ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <?php if (!$datas) { ?>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Company found</p>
                <?php  } ?>
            </div>
            <div class="pagination-dive col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">
                <?php echo $nav; ?>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center padding_left_null padding_right_null">
            <a href="<?php echo base_url(); ?>company" class="btn btn-info" role="button">Add New Company</a>
        </div>
    </div>
</div>
