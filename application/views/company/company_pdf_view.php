<div class="col-md-12">
    <div class="col-md-4">
        <p style="text-align: left;font-weight: bold;font-style: italic;">Gopal Tex</p>
    </div>
    <div style="text-align: center" class="col-md-4">
        <img style="width: 25%" src="<?php echo site_url() ?>skin/image/gopaltex.png" alt="gopal-tex"
             title="gopal-tex"/>
    </div>
    <div class="col-md-4">
        <p style="text-align: right;font-weight: bold;font-style: italic;"><?php echo date("d/m/Y"); ?></p>
<!--        <p style="text-align: right;font-weight: bold;font-style: italic;"> Serial No: --><?php //echo $pdf_id; ?><!--</p>-->
    </div>
</div>
<div class="col-md-12">
    <p style="text-align: center;font-weight: bold"><?php echo $title; ?></p>
</div>

<div class="col-md-12 margin_top_ten">
    <?php if ($datas) { ?>
        <table id="customers" class="col-md-12 margin_top_ten">
            <thead>
            <tr>
                <th>Id</th>
                <th>Company Name</th>
                <th>Contact Name</th>
                <th>Contact No1</th>
                <th>Contact No2</th>
                <th>Address</th>
                <th>Gst</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($datas) && is_array($datas) && count($datas)) {
                $i = 0;
                foreach ($datas as $data) {
                    ?>
                    <tr>
                        <td><?php echo $data["id"]; ?></td>
                        <td><?php echo $data["company_name"]; ?></td>
                        <td><?php echo $data["contact_name"]; ?></td>
                        <td><?php echo $data["contact_num_1"]; ?></td>
                        <td><?php echo $data["contact_num_2"]; ?></td>
                        <td><?php echo $data["address"]; ?></td>
                        <td><?php echo $data["gst"]; ?></td>
                        <td><?php echo $data["status"]; ?></td>
                    </tr>
                    <?php
                    $i++;
                }
            }
            ?>
            </tbody>
        </table>

    <?php } ?>

    <?php if (!$datas) { ?>
        <p class="col-md-12">No Company found</p>
    <?php } ?>

</div>

<div class="col-md-12 text-center margin_top_ten">
    <a href="<?php echo base_url(); ?>company/view" class="btn btn-info" role="button">Back</a>
</div>

<style>
    .col-md-12 {
        width: 100%;
        float: left;
    }

    .col-md-12 {
        width: 100%;
        float: left;
    }

    .col-md-6 {
        width: 50%;
        float: left;
    }

    .col-md-4 {
        width: 33.33%;
        float: left;
    }

    .col-md-3 {
        width: 25%;
        float: left;
    }

    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #D2C3C3;
        color: white;
    }

    .text-center {
        text-align: center;
    }

    .margin_top_ten {
        margin-top: 10px;
    }

    .margin_bottom_ten {
        margin-bottom: 10px;
    }

    .margin_top_bottom {
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>