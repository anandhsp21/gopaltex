<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Payment Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('wage'); ?>">Wage Payment</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('intermediate/view'); ?>">Intermediate Payment</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('supplier/view'); ?>">Supplier Payment</a>
                    </li>
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('payreport/view'); ?>">Payment Report</a>
                    </li>
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('wage/invoice'); ?>">Paid invoice</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Edit Intermediate Payment Management</span></p>
        </div>

        <?php if (isset($edit_data) && is_array($edit_data) && count($edit_data)){ ?>
        <form method="post" action="<?php echo site_url('intermediate/update'); ?>" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>"/>

            <div class="padding_left_ten padding_right_ten padding_top_ten padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desig">Designation:</label>
                        <select required class="selectpicker form-control wage_click" id="desig" name="desig">
                            <option value="">--Designation--</option>
                            <option <?php if ($edit_data['desig'] == 'weaver') {
                                echo 'selected';
                            } ?> value="weaver">Weaver
                            </option>
                            <option <?php if ($edit_data['desig'] == 'warping') {
                                echo 'selected';
                            } ?> value="warping">Warping
                            </option>
                            <option <?php if ($edit_data['desig'] == 'winding') {
                                echo 'selected';
                            } ?> value="winding">Winding
                            </option>
                            <option <?php if ($edit_data['desig'] == 'hooking') {
                                echo 'selected';
                            } ?> value="hooking">Hooking
                            </option>
                            <option <?php if ($edit_data['desig'] == 'knitting') {
                                echo 'selected';
                            } ?> value="knitting">Knitting
                            </option>
                            <option <?php if ($edit_data['desig'] == 'pinner') {
                                echo 'selected';
                            } ?> value="pinner">Pinner
                            </option>
                            <option <?php if ($edit_data['desig'] == 'fitter') {
                                echo 'selected';
                            } ?> value="fitter">Fitter
                            </option>
                            <option <?php if ($edit_data['desig'] == 'manager') {
                                echo 'selected';
                            } ?> value="manager">Manager
                            </option>
                        </select>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="date">Date:</label>
                        <div class="input-group date doj" data-provide="datepicker">
                            <input type="text" value="<?php echo $edit_data['date']; ?>" required placeholder="Date" name="date" id="date"
                                   class="form-control">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Amount">Amount:</label>
                        <input type="text" value="<?php echo $edit_data['amount']; ?>" required placeholder="Amount" class="form-control"
                               name="amount" id="amount">
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="emp_id">Emp Name:</label>
                        <select required class="selectpicker form-control wage_emp_name" id="emp_id" name="emp_id">
                            <option value="">-- <?php if (!$employee_ids || empty($employee_ids)) {
                                    echo 'No';
                                } ?> Employee Name--
                            </option>
                            <?php
                            if (isset($employee_ids)) {
                                $i = 0;
                                foreach ($employee_ids as $employee_id) {  ?>
                                    <option <?php if (intval(@$employee_id['id']) == intval($edit_data['emp_id'])) {
                                        echo 'selected';
                                    } ?> value="<?php echo $employee_id['id'] ?>"><?php echo $employee_id['emp_name'] ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="purpose">Purpose:</label>
                        <input type="text" value="<?php echo $edit_data['purpose']; ?>" required placeholder="Purpose" class="form-control"
                               name="purpose" id="purpose">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_bottom_ten">
                        <label for="status">Status:</label>
                        <select required class="selectpicker form-control" id="status" name="status">
                            <option value="">--Status--</option>
                            <option <?php if ($edit_data['status'] == 'tallied') {
                                echo 'selected';
                            } ?> value="tallied">Tally
                            </option>
                            <option <?php if ($edit_data['status'] == 'not_tallied') {
                                echo 'selected';
                            } ?> value="not_tallied">Not Tally
                            </option>
                        </select>
                    </div>

                </div>

            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>intermediate/view" class="btn btn-info" role="button">Back</a>
                <button type="submit" class="btn btn-primary text-center">Submit</button>
            </div>
            <?php } ?>
        </form>
    </div>
</div>
