<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Menu</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12 margin_top_ten">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('company'); ?>">Company</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry'); ?>">Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders'); ?>">Orders</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link"
                           href="<?php echo site_url('employees'); ?>">Employee</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_left_null padding_right_null text-center quick_dashboard_mobile quick_dashboard">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Total Records</span></p>
        </div>
        <div class="margin_top_ten col-md-12 col-sm-12 col-xl-12 col-xs-12 quick_dashboard_mobile quick_dashboard_mobile_right">

            <div class="counter wow bounceInUp col-lg-3 col-md-6 col-sm-6 col-xs-12 quick_dashboard_mobile quick_dashboard_mobile_right padding_top_ten padding_bottom_ten">
                <a href="<?php echo site_url('company'); ?>">
                    <div class="employees col-md-12 col-sm-12 col-xl-12 col-xs-12">
                        <p class="counter-count rotate"><?php echo $count['company_data_count'] ?></p>
                        <p class="employee-p">Company</p>
                    </div>
                </a>
            </div>

            <div class="counter wow bounceInDown col-lg-3 col-md-6 col-sm-6 col-xs-12 quick_dashboard_mobile quick_dashboard_mobile_right padding_top_ten padding_bottom_ten">
                <a href="<?php echo site_url('entry'); ?>">
                    <div class="customer col-md-12 col-sm-12 col-xl-12 col-xs-12">
                        <p class="counter-count rotate"><?php echo $count['entry_data'] ?></p>
                        <p class="customer-p">Entry</p>
                    </div>
                </a>
            </div>

            <div class="counter wow bounceInUp col-lg-3 col-md-6 col-sm-6 col-xs-12 quick_dashboard_mobile quick_dashboard_mobile_right padding_top_ten padding_bottom_ten">
                <a href="<?php echo site_url('orders'); ?>">
                    <div class="design col-md-12 col-sm-12 col-xl-12 col-xs-12">
                        <p class="counter-count rotate"><?php echo $count['order_data_count'] ?></p>
                        <p class="design-p">Orders</p>
                    </div>
                </a>
            </div>

            <div class="counter wow bounceInDown col-lg-3 col-md-6 col-sm-6 col-xs-12 quick_dashboard_mobile quick_dashboard_mobile_right padding_top_ten padding_bottom_ten">
                <a href="<?php echo site_url('employees'); ?>">
                    <div class="order col-md-12 col-sm-12 col-xl-12 col-xs-12">
                        <p class="counter-count rotate"><?php echo $count['employee_data_count'] ?></p>
                        <p class="order-p">Employee</p>
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>
