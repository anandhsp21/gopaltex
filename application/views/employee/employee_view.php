<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Employee Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('employees'); ?>">Add Employee</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('employees/view'); ?>">View Employee</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('employees/report'); ?>">Employee Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">View Employee</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title margin_top_ten col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_right_null padding_left_null">
                <form method="post" action="<?php echo base_url(); ?>employees/view">
                    <div class="form-group search_button col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 reset_empty">
                            <input type="text" placeholder="Employee Name" class="form-control" id="title"
                                   name="emp_name"
                                   value="<?php if (!empty($search_title)) {
                                       echo $search_title;
                                   } ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input type="submit" name="search" class="btn btn-primary">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input type="button" value="Reset" class="reset_button btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom table_over_flow padding_right_null padding_left_null">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Mobile</th>
                        <th>Advance</th>
                        <th>Image</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (isset($datas) && is_array($datas) && count($datas)) {
                        $i = 0;
                        foreach ($datas as $data) {
                            ?>
                            <tr>
                                <td><?php echo $data["id"]; ?></td>
                                <td><?php echo $data["emp_name"]; ?></td>
                                <td><?php echo $data["father_name"]; ?></td>
                                <td><?php echo $data["mobile"]; ?></td>
                                <td><?php echo $data["advance"]; ?></td>
                                <td class="emp_view_profile">
                                    <?php if ($data['employee_image']) { ?>
                                        <img src="<?php echo base_url() ?>/uploads/employee/<?php echo $data["employee_image"]; ?>"
                                             alt="<?php echo $data["emp_name"]; ?>"
                                             title="<?php echo $data["emp_name"]; ?>">
                                    <?php } else { ?>
                                        No Profile Picture
                                    <?php } ?>
                                </td>
                                <td><a href="<?php echo site_url('employees/edit/' . $data['id'] . ''); ?>">View</a>
                                </td>
                                <td><a href="<?php echo site_url('employees/delete/' . $data['id'] . ''); ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <?php if (!$datas) { ?>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Employee found</p>
                <?php  } ?>
            </div>
            <div class="pagination-dive col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $nav; ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>employees" class="btn btn-info" role="button">Add New Employee</a>
            </div>
        </div>
    </div>
</div>
