<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Employee Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('employees'); ?>">Add Employee</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('employees/view'); ?>">View Employee</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('employees/report'); ?>">Employee Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Add Employee</span></p>
        </div>

        <form method="post" action="<?php echo site_url('employees/create'); ?>" enctype="multipart/form-data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">
                    <div class="form-group padding_top_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="emp_name">Employee Name:</label>
                        <input type="text" required placeholder="Employee Name" class="form-control" name="emp_name"
                               id="emp_name">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="father_name">Father Name:</label>
                        <input type="text" required placeholder="Father Name" class="form-control" name="father_name"
                               id="father_name">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="age">Age:</label>
                        <input type="text" required placeholder="Age" class="form-control" name="age" id="age">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desig">Designation:</label>
                        <select required class="selectpicker form-control" id="desig" name="desig">
                            <option value="">--Designation--</option>
                            <option value="weaver">Weaver</option>
                            <option value="warping">Warping</option>
                            <option value="winding">Winding</option>
                            <option value="hooking">Hooking</option>
                            <option value="knitting">Knitting</option>
                            <option value="pinner">Pinner</option>
                            <option value="fitter">Fitter</option>
                            <option value="manager">Manager</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="mobile">Mobile No:</label>
                        <input type="text" required placeholder="Mobile No" class="form-control" name="mobile"
                               id="mobile">
                    </div>
                    <div class="form-group padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="doj">Date of Join:</label>

                        <div class="input-group date doj" data-provide="datepicker">
                            <input type="text" name="doj" id="doj" class="form-control">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">
                    <div class="form-group padding_top_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="advance">Advance:</label>
                        <input type="text" required placeholder="Advance" class="form-control" name="advance"
                               id="advance">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="account_no">Account No:</label>
                        <input type="text" required placeholder="Account No" class="form-control" name="account_no"
                               id="account_no">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="ifsc">Ifsc:</label>
                        <input type="text" required placeholder="Ifsc" class="form-control" name="ifsc" id="ifsc">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="aadhar_id">Aadhar Id:</label>
                        <input type="text" required placeholder="Aadhar Id" class="form-control" name="aadhar_id"
                               id="aadhar_id">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="balance">Balance:</label>
                        <input type="text" required placeholder="Balance" class="form-control" name="balance"
                               id="balance">
                    </div>
                    <div class="form-group padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="status">Status:</label>
                        <select required class="selectpicker form-control" id="status" name="status">
                            <option value="">--Status--</option>
                            <option value="active">Active</option>
                            <option value="inactive">In Active</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_left_null padding_top_bottom pull-left">
                            <label class="pull-left">Employee Photo</label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom">
                            <input type="file" name="userfile" size="20"/>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 alert alert-warning padding_top_bottom">
                            <strong>Warning!</strong> Allowed files types gif,jpg,png
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>employees/view" class="btn btn-info" role="button">Back</a>
                <button type="submit" class="btn btn-primary text-center">Submit</button>
            </div>
        </form>
    </div>
</div>
