<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer_text container-fluid">
    <p class="text-center wow animated pulse">All Rights Reserved © <a href="<?php echo site_url();?>"> www.gopaltex.in </a> <?php echo date("Y"); ?></p>
</div>
<?php if ($this->session->flashdata('message')) { ?>
    <div class="toast">
        <div class="alert alert-info">
            <strong><?php echo $this->session->flashdata('message') ?></strong>
        </div>
    </div>
<?php } ?>
</body>
</html>