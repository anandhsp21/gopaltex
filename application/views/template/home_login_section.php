<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid header_colour">
    <div class="col-lg-5 col-sm-5 col-md-6 col-xs-12 as_logined">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="wow fadeInLeft"> Gopal Tex Power Loom</h3>
        </div>
    </div>
    <div class="col-lg-7 col-sm-7 col-md-6 col-xs-12 logo">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
            <a class="wow bounceInDown" href="<?php echo site_url('home');?>">
                <img class="wow animated pulse" data-wow-delay="0.75s" src="<?php echo site_url() ?>skin/image/gopaltex.png" alt="gopal-tex" title="gopal-tex"/>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 logined_controls padding_left_null padding_right_null">
            <?php $value = $this->session->userdata('username'); ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a class="wow bounceInRight" data-wow-delay="0.2s" href="">Hello, <?php if (isset($value)) {echo $value;} ?> </a>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a class="wow bounceInRight" data-wow-delay="0.3s" id="time" href=""></a>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a class="wow bounceInRight" data-wow-delay="0.4s" href="<?php echo site_url('users/logout'); ?>"> logout</a>
            </div>
        </div>
        <script>
            function t() {
                document.getElementById('time').innerHTML = new Date().toLocaleTimeString();
            }
            t();
            window.setInterval(t, 1000);
        </script>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 container-fluid col-xs-12 dashboard_menu padding_top_ten">
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand wow fadeInLeft" href="<?php echo site_url('home'); ?>">Gopal Tex</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li>
                    <a class="nav-link wow fadeInDown" href="<?php echo site_url('home'); ?>">Home</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInUp" href="<?php echo site_url('company'); ?>">Company</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInDown" href="<?php echo site_url('entry'); ?>">Entry</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInUp" href="<?php echo site_url('orders'); ?>">Orders</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInDown" href="<?php echo site_url('employees'); ?>">Employees</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInUp" href="<?php echo site_url('wage'); ?>">Payment</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInUp" href="<?php echo site_url('deliverin'); ?>">Delivers In</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInDown" href="<?php echo site_url('home'); ?>">Suppliers</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInDown" href="<?php echo site_url('home'); ?>">Delivery Out</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInUp" href="<?php echo site_url('home'); ?>">Outside Workers</a>
                </li>
                <li>
                    <a class="nav-link wow fadeInDown" href="<?php echo site_url('users'); ?>">Users</a>
                </li>
            </ul>
        </div>
    </nav>
</div>