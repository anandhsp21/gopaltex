<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid header_colour">
    <div class="container">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 padding_top_bottom main_header">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <span class="dashboard_icon wow animated fadeInLeft">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>
            <div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-12">
                <h3 class="wow animated pulse"> LOGIN TO DASHBOARD </h3>
                <p class="wow animated pulse">Enter your credentials below</p>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 logo wow animated fadeInRight">
                <a href="<?php echo site_url(); ?>">
                    <img class="img-rounded" src="<?php echo site_url() ?>skin/image/gopaltex.png" alt="gopal-tex" title="gopal-tex"/>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">
    <h2 class="text-center wow animated pulse">Please login into continue</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
        <form method="post" action="<?php echo site_url('home/login_data'); ?>" class="col-lg-4 form_bg col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="text" class="form-control" id="username" name="username"
                               placeholder="username">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="password">
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-primary text-center wow shake animated">Login</button>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_info text-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow animated fadeInDown">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                    <p>Just click on the "LOG IN" button to continue with login information. </p>
                </div>
            </div>
        </form>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>