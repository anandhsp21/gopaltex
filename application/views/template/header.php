<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0"/>
    <meta name="keywords" content="SRI GOPAL TEX, Gopal Textile, Power loom in Namakkal, Power loom in Tiruchengode, Power loom in Rasipuram, Power loom in Jedarpalayam, Power loom in salem, Gopal Tex">
    <title>Gopal Tex</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/bootstrap-select.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/bootstrap-datepicker.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/css/responsive.css"/>
    <link rel="icon" href="<?php echo base_url(); ?>skin/image/gopaltex.png" sizes="16x16">
    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>skin/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>skin/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>skin/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>skin/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>skin/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>skin/js/custom.js"></script>
</head>
<body>
