<div class="col-md-12">
    <div class="col-md-4">
        <p style="text-align: left;font-weight: bold;font-style: italic;">Gopal Tex</p>
    </div>
    <div style="text-align: center" class="col-md-4">
        <img style="width: 25%" src="<?php echo site_url() ?>skin/image/gopaltex.png" alt="gopal-tex"
             title="gopal-tex"/>
    </div>
    <div class="col-md-4">
        <p style="text-align: right;font-weight: bold;font-style: italic;"><?php echo date("d/m/Y"); ?></p>
        <p style="text-align: right;font-weight: bold;font-style: italic;"> Serial No: <?php echo $pdf_id; ?></p>
    </div>
</div>
<div class="col-md-12">
    <p style="text-align: center;font-weight: bold">Payment Report</p>
</div>
<div class="col-md-12">
    <div class="col-md-3" style="text-align: left">
        <p> Name : <?php echo $emp_name; ?></p>
    </div>
    <div class="col-md-3" style="text-align: center">
        <p> From Date: <?php echo $from_date; ?></p>
    </div>
    <div class="col-md-3" style="text-align: center">
        <p> To Date: <?php echo $to_date; ?></p>
    </div>
    <div class="col-md-3" style="text-align: right">
        <p> Desig: <?php echo $desig; ?></p>
    </div>
</div>
<?php $final_cost = 0; ?>

<div class="col-md-12 margin_top_ten">
    <?php if ($datas) { ?>
        <table id="customers" class="col-md-12 margin_top_ten">
            <thead>
            <tr>
                <th>Id</th>
                <th>Date</th>
                <?php if ($db_name == 'weaving_entry_data') { ?>
                    <th>Shift</th>
                    <th>Loom No</th>
                <?php } ?>
                <?php if ($db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data' || $db_name == 'pin_entry_data') { ?>
                    <th>Loom No</th>
                <?php } ?>
                <?php if ($db_name == 'warping_entry_data') { ?>
                    <th>Machine No</th>
                <?php } ?>
                <th>Order Name</th>
                <?php if ($db_name == 'weaving_entry_data' || $db_name == 'warping_entry_data') { ?>
                    <th>Metres</th>
                <?php } ?>
                <th>Cost</th>
                <?php if ($db_name != 'pin_entry_data' || $db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data') { ?>
                    <th>Total Cost</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($datas) && is_array($datas) && count($datas)) {
                $i = 0;
                foreach ($datas as $data) {
                    ?>
                    <tr>
                        <td><?php echo $data["id"]; ?></td>
                        <td><?php echo $data["date"]; ?></td>
                        <?php if ($db_name == 'weaving_entry_data') { ?>
                            <td><?php echo $data["shift"]; ?></td>
                            <td><?php echo $data["loom_no"]; ?></td>
                        <?php } ?>
                        <?php if ($db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data' || $db_name == 'pin_entry_data') { ?>
                            <td><?php echo $data["loom_no"]; ?></td>
                        <?php } ?>
                        <?php if ($db_name == 'warping_entry_data') { ?>
                            <td><?php echo $data["machine_no"]; ?></td>
                        <?php } ?>
                        <td>
                            <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                foreach ($orders_ids as $order_id) {
                                    if (intval($order_id['id']) == intval($data['order_id'])) {
                                        echo $order_id["order_name"];
                                    }
                                }
                            } ?>
                        </td>
                        <?php if ($db_name == 'weaving_entry_data' || $db_name == 'warping_entry_data') { ?>
                            <td><?php echo $data["meters"]; ?></td>
                        <?php } ?>
                        <td>
                            <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                foreach ($orders_ids as $order_id) {
                                    if (intval($order_id['id']) == intval($data['order_id'])) {
                                        echo $order_id[$col_name];
                                    }
                                }
                            } ?>
                        </td>
                        <?php if ($db_name == 'weaving_entry_data' || $db_name == 'warping_entry_data') { ?>
                            <td>
                                <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                    foreach ($orders_ids as $order_id) {
                                        if (intval($order_id['id']) == intval($data['order_id'])) {
                                            $single_cost = $order_id[$col_name] * $data["meters"];
                                            $final_cost += $single_cost;
                                            echo $single_cost;
                                        }
                                    }
                                } ?>
                            </td>
                        <?php } else if ($db_name == 'pin_entry_data' || $db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data') { ?>
                            <td>
                                <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                    foreach ($orders_ids as $order_id) {
                                        if (intval($order_id['id']) == intval($data['order_id'])) {
                                            $single_cost = $order_id[$col_name];
                                            $final_cost += $single_cost;
                                            echo $single_cost;
                                        }
                                    }
                                } ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <?php
                    $i++;
                }
            }
            ?>
            </tbody>
        </table>

        <div class="col-md-12">
            <div class="col-md-4">
                <p>Current Balance : <?php echo $cbalances; ?></p>
            </div>
            <div class="col-md-4">
                <p>Paid Amount : <?php echo $paid_amount; ?></p>
            </div>
            <div class="col-md-4">
                <p>Total Wages : <?php echo $final_cost; ?></p>
                <p>Intermediate Payment : <?php echo $intermediate_payment; ?></p>
                <p>Previous Balance : <?php echo $prev_balance; ?></p>
                <p>Extras : <?php echo $extras; ?></p>
            </div>
        </div>
    <?php } ?>

    <?php if (!$datas) { ?>
        <p class="col-md-12">No Entries found</p>
    <?php } ?>

</div>

<div class="col-md-12">
    <div class="col-md-4">
        &nbsp;
    </div>
    <div class="col-md-4">
        &nbsp;
    </div>
    <div class="col-md-4">
        <p class="net_amount_to_pay">Net amount to pay : <span
                    class="final_net_amount"><?php echo(intval($final_cost) - intval($intermediate_payment) + intval($prev_balance) + intval($extras)); ?></span>
        </p>
    </div>
</div>

<div class="col-md-12 text-center">
    <a href="<?php echo base_url(); ?>wage/invoice" class="btn btn-info" role="button">Back</a>
</div>

<style>
    .col-md-12 {
        width: 100%;
        float: left;
    }

    .col-md-12 {
        width: 100%;
        float: left;
    }

    .col-md-6 {
        width: 50%;
        float: left;
    }

    .col-md-4 {
        width: 33.33%;
        float: left;
    }

    .col-md-3 {
        width: 25%;
        float: left;
    }

    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #D2C3C3;
        color: white;
    }

    .text-center {
        text-align: center;
    }

    .margin_top_ten {
        margin-right: 10px;
    }

    .margin_bottom_ten {
        margin-bottom: 10px;
    }

    .margin_top_bottom {
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>