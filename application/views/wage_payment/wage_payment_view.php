<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Payment Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('wage'); ?>">Wage Payment</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('intermediate/view'); ?>">Intermediate Payment</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('supplier/view'); ?>">Supplier Payment</a>
                    </li>
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('payreport/view'); ?>">Payment Report</a>
                    </li>
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('wage/invoice'); ?>">Paid invoice</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">View Payment Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title margin_top_ten col-xs-12 padding_left_null padding_right_null">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <p>Employee Name : <?php echo $emp_name; ?> </p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <p>From Date: <?php echo $from_date; ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <p>To Date: <?php echo $to_date; ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <p>Desig: <?php echo $desig; ?></p>
                    </div>
                </div>
            </div>
            <?php $final_cost = 0; ?>
            <form method="post" action="<?php echo base_url(); ?>wage/calculate_wage">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom table_over_flow">
                    <?php if ($datas) { ?>
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Date</th>
                                <?php if ($db_name == 'weaving_entry_data') { ?>
                                    <th>Shift</th>
                                    <th>Loom No</th>
                                <?php } ?>
                                <?php if ($db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data' || $db_name == 'pin_entry_data') { ?>
                                    <th>Loom No</th>
                                <?php } ?>
                                <?php if ($db_name == 'warping_entry_data') { ?>
                                    <th>Machine No</th>
                                <?php } ?>
                                <th>Order Name</th>
                                <?php if ($db_name == 'weaving_entry_data' || $db_name == 'warping_entry_data') { ?>
                                    <th>Metres</th>
                                <?php } ?>
                                <th>Cost</th>
                                <?php if ($db_name != 'pin_entry_data' || $db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data') { ?>
                                    <th>Total Cost</th>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (isset($datas) && is_array($datas) && count($datas)) {
                                $i = 0;
                                foreach ($datas as $data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $data["id"]; ?></td>
                                        <td><?php echo $data["date"]; ?></td>
                                        <?php if ($db_name == 'weaving_entry_data') { ?>
                                            <td><?php echo $data["shift"]; ?></td>
                                            <td><?php echo $data["loom_no"]; ?></td>
                                        <?php } ?>
                                        <?php if ($db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data' || $db_name == 'pin_entry_data') { ?>
                                            <td><?php echo $data["loom_no"]; ?></td>
                                        <?php } ?>
                                        <?php if ($db_name == 'warping_entry_data') { ?>
                                            <td><?php echo $data["machine_no"]; ?></td>
                                        <?php } ?>
                                        <td>
                                            <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                                foreach ($orders_ids as $order_id) {
                                                    if (intval($order_id['id']) == intval($data['order_id'])) {
                                                        echo $order_id["order_name"];
                                                    }
                                                }
                                            } ?>
                                        </td>
                                        <?php if ($db_name == 'weaving_entry_data' || $db_name == 'warping_entry_data') { ?>
                                            <td><?php echo $data["meters"]; ?></td>
                                        <?php } ?>
                                        <td>
                                            <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                                foreach ($orders_ids as $order_id) {
                                                    if (intval($order_id['id']) == intval($data['order_id'])) {
                                                        echo $order_id[$col_name];
                                                    }
                                                }
                                            } ?>
                                        </td>
                                        <?php if ($db_name == 'weaving_entry_data' || $db_name == 'warping_entry_data') { ?>
                                            <td>
                                                <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                                    foreach ($orders_ids as $order_id) {
                                                        if (intval($order_id['id']) == intval($data['order_id'])) {
                                                            $single_cost = $order_id[$col_name] * $data["meters"];
                                                            $final_cost += $single_cost;
                                                            echo $single_cost;
                                                        }
                                                    }
                                                } ?>
                                            </td>
                                        <?php } else if ($db_name == 'pin_entry_data' || $db_name == 'hook_entry_data' || $db_name == 'knotting_entry_data') { ?>
                                            <td>
                                                <?php if (isset($orders_ids) && is_array($orders_ids) && count($orders_ids)) {
                                                    foreach ($orders_ids as $order_id) {
                                                        if (intval($order_id['id']) == intval($data['order_id'])) {
                                                            $single_cost = $order_id[$col_name];
                                                            $final_cost += $single_cost;
                                                            echo $single_cost;
                                                        }
                                                    }
                                                } ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
                <?php if ($datas) { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom padding_right_null padding_left_null text-center">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom padding_right_null padding_left_null">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom table_over_flow">
                                <p>Current Balance : <span class="cbalance"> 0 </span></p>
                                <input type="hidden" value="" required placeholder="Extras"
                                       class="form-control cbalances" name="cbalances" id="cbalances">

                                <p class="net_amount_to_pay margin_top_ten">Net amount to pay : <span
                                            class="final_net_amount"><?php echo(intval($final_cost) - intval($intermediate_payment) + intval($prev_balance)); ?></span>
                                </p>

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom table_over_flow">

                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="paid_amount">Paid Amount:</label>
                                    <input type="number" value="" min="0" required placeholder="Paid Amount"
                                           class="form-control paid_amount"
                                           name="paid_amount" id="paid_amount">
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="extras">Extras:</label>
                                    <input type="number" value="" min="0" required placeholder="Extras"
                                           class="form-control extras" name="extras" id="extras">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom table_over_flow">
                                <p class="">Total Wages : <?php echo $final_cost; ?></p>
                                <p class="margin_top_ten">Intermediate Payment
                                    : <?php echo $intermediate_payment; ?></p>
                                <p class="margin_top_ten">Previous Balance : <?php echo $prev_balance; ?></p>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom padding_right_null padding_left_null">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom table_over_flow">
                                <input type="hidden" value="<?php echo $final_cost; ?>"
                                       class="form-control final_cost" name="final_cost" id="final_cost">
                                <input type="hidden" value="<?php echo $intermediate_payment; ?>"
                                       class="form-control intermediate_payment" name="intermediate_payment"
                                       id="intermediate_payment">
                                <input type="hidden" value="<?php echo $prev_balance; ?>"
                                       class="form-control prev_balance" name="prev_balance" id="prev_balance">
                                <input type="hidden" value="<?php echo $emp_id; ?>" class="form-control emp_id"
                                       name="emp_id" id="emp_id">
                                <input type="hidden" value="<?php echo $from_date; ?>"
                                       class="form-control from_date" name="from_date" id="from_date">
                                <input type="hidden" value="<?php echo $to_date; ?>" class="form-control to_date"
                                       name="to_date" id="to_date">
                                <input type="hidden" value="<?php echo $desig; ?>" class="form-control desig"
                                       name="desig" id="desig">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom table_over_flow">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom table_over_flow">

                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (!$datas) { ?>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Entries
                        found</p>
                <?php } ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding_top_bottom padding_right_null padding_left_null">
                    <a href="<?php echo base_url(); ?>wage" class="btn btn-info" role="button">Back</a>
                    <?php if ($datas) { ?>
                        <button type="submit" class="btn btn-primary text-center">Pay Wages</button>
                    <?php } ?>
                </div>
            </form>
        </div>
    </div>
</div>
