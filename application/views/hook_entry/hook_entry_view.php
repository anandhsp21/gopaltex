<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Entry Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry'); ?>">Weaving Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('warping/view'); ?>">Warping Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('knotting/view'); ?>">Knotting Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('pin/view'); ?>">Pin Entry</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('hook/view'); ?>">Hook Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('winding/view'); ?>">Winding Entry</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">View Pin Entry</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title margin_top_ten col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null">
                <form method="post" action="<?php echo base_url(); ?>hook/view">
                    <div class="form-group search_button col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="input-group date doj" data-provide="datepicker">
                                <input placeholder="search date" value="<?php if (!empty($search_date)) {
                                    echo $search_date;
                                } ?>" type="text" name="date" id="date" class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <input type="submit" name="search" class="btn btn-primary">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <input type="button" value="Reset" class="reset_button btn btn-primary">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <a href="<?php echo base_url(); ?>hook/create" class="btn btn-info" role="button">Add Hook Entry</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom table_over_flow padding_left_null padding_right_null">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Date</th>
                        <th>Loom No</th>
                        <th>Emp Name</th>
                        <th>Order Name</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (isset($datas) && is_array($datas) && count($datas)) {
                        $i = 0;
                        foreach ($datas as $data) {
                            ?>
                            <tr>
                                <td><?php echo $data["id"]; ?></td>
                                <td><?php echo $data["date"]; ?></td>
                                <td><?php echo $data["loom_no"]; ?></td>
                                <td>
                                    <?php if(isset($employee_ids) && is_array($employee_ids) && count($employee_ids)){
                                        foreach ($employee_ids as $employee_id) {
                                            if(intval($employee_id['id']) == intval($data['emp_id'])){
                                                echo $employee_id["emp_name"];
                                            }
                                        }
                                    } ?>
                                </td>
                                <td>
                                    <?php if(isset($orders_ids) && is_array($orders_ids) && count($orders_ids)){
                                        foreach ($orders_ids as $order_id) {
                                            if(intval($order_id['id']) == intval($data['order_id'])){
                                                echo $order_id["order_name"];
                                            }
                                        }
                                    } ?>
                                </td>
                                <td><a href="<?php echo site_url('hook/edit/' . $data['id'] . ''); ?>">View</a>
                                </td>
                                <td><a href="<?php echo site_url('hook/delete/' . $data['id'] . ''); ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <?php if (!$datas) { ?>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Pin Entries found</p>
                <?php  } ?>
            </div>
            <div class="pagination-dive col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $nav; ?>
            </div>
        </div>
    </div>
</div>
