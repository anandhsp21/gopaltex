<div id="container" class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 add_form">
            <h2 class="text-center">Add New user </h2>
            <form method="post" action="<?php echo site_url('users/admin_submit_data'); ?>"
                  enctype="multipart/form-data">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label>Title</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <input type="text" class="form-control" name="name" required/>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label>description</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <textarea name="description" class="form-control" rows="6" required></textarea>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label>Document</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <input type="file" name="userfile" size="20"/>
                    </div>
                    <div class="alert alert-warning">
                        <strong>Warning!</strong> Allowed files types gif,jpg,png
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                    <a href="<?php echo site_url('users/index'); ?>" class="btn btn-info" role="button">Back</a>
                    <button type="submit" class="btn btn-primary text-center">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>