<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Entry Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry'); ?>">Weaving Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry/view'); ?>">Warping Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('knotting/view'); ?>">Knotting Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('pin/view'); ?>">Pin Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('hook/view'); ?>">Hook Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('winding/view'); ?>">Winding Entry</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Weaving Entry</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding multiple_form_create">
            <form method="post" action="<?php echo site_url('entry/multiple_create_insert'); ?>"
                  enctype="multipart/form-data">
                <div class="form-group padding_left_ten padding_right_ten padding_bottom_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label for="weaving_entry">Weaving Entry:</label>
                    <select required class="selectpicker form-control" id="weaving_entry" name="weaving_entry">
                        <option selected value="multiple">Multiple Entry</option>
                    </select>
                </div>

                <?php for ($i = 0; $i < intval($no_of_entries); $i++) { ?>

                    <input type="hidden" id="date_<?php echo $i; ?>" name="date[]" value="<?php echo $date; ?>" />
                    <input type="hidden" id="shift_<?php echo $i; ?>" name="shift[]" value="<?php echo $shift; ?>" />

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 back_bg">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="loom_no_<?php echo $i; ?>">Loom No:</label>
                                    <input type="text" required placeholder="Loom No" class="form-control" value="<?php echo $i+1; ?>"
                                           name="loom_no[]" id="loom_no_<?php echo $i; ?>">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="emp_id_<?php echo $i; ?>">Emp Name:</label>
                                    <select required class="selectpicker form-control" id="emp_id_<?php echo $i; ?>"
                                            name="emp_id[]">
                                        <option value="">-- <?php if (!$employee_ids || empty($employee_ids)) {
                                                echo 'No';
                                            } ?> Employee Name--
                                        </option>
                                        <?php
                                        if (isset($employee_ids)) {
                                            foreach ($employee_ids as $employee_id) { ?>
                                                <option value="<?php echo $employee_id['id'] ?>"><?php echo $employee_id['emp_name'] ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="order_id_<?php echo $i; ?>">Order Name:</label>
                                    <select required class="selectpicker form-control" id="order_id_<?php echo $i; ?>"
                                            name="order_id[]">
                                        <option value="">-- <?php if (!$orders_ids || empty($orders_ids)) {
                                                echo 'No';
                                            } ?> Order Name--
                                        </option>
                                        <?php
                                        if (isset($orders_ids)) {
                                            foreach ($orders_ids as $orders_id) { ?>
                                                <option value="<?php echo $orders_id['id'] ?>"><?php echo $orders_id['order_name'] ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="meters_<?php echo $i; ?>">Meters:</label>
                                    <input type="text" required placeholder="Meters" class="form-control" name="meters[]"
                                           id="meters_<?php echo $i; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                    <a href="<?php echo base_url(); ?>entry/multiple_create" class="btn btn-info" role="button">Back</a>
                    <button type="submit" class="btn btn-primary text-center">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
