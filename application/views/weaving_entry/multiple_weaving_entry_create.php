<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Entry Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry'); ?>">Weaving Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('entry/view'); ?>">Warping Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('knotting/view'); ?>">Knotting Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('pin/view'); ?>">Pin Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('hook/view'); ?>">Hook Entry</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('winding/view'); ?>">Winding Entry</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Weaving Entry</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
            <form method="post" action="<?php echo site_url('entry/multiple_create_insert_loop'); ?>" enctype="multipart/form-data">

                <div class="padding_left_ten padding_right_ten padding_bottom_ten padding_left_null padding_right_null">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="weaving_entry">Weaving Entry:</label>
                        <select required class="selectpicker form-control" id="weaving_entry" name="weaving_entry">
                            <option selected value="multiple">Multiple Entry</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding padding_left_null padding_right_null">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="date">Date:</label>
                            <div class="input-group date doj" data-provide="datepicker">
                                <input type="text" required placeholder="Date" name="date" id="date"
                                       class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="shift">Shift:</label>
                            <select required class="selectpicker form-control" id="shift" name="shift">
                                <option value="">--Shift--</option>
                                <option value="day">Day</option>
                                <option value="night">Night</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="no_of_entries">No of Entries:</label>
                            <input type="number" min="1" max="20" required placeholder="No of Entries"
                                   class="form-control" name="no_of_entries" id="no_of_entries">
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center padding_left_null padding_right_null">
                    <a href="<?php echo base_url(); ?>entry/weaving_entry_view" class="btn btn-info" role="button">Back</a>
                    <button type="submit" class="btn btn-primary text-center">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
