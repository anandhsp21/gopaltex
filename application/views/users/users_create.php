<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p>User Management</p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('users'); ?>">Add User</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('users/view'); ?>">View User</a>
                    </li>
<!--                    <li class="nav-item">-->
<!--                        <a class="wow fadeIn animated nav-link" href="--><?php //echo site_url('users/report'); ?><!--">User Report</a>-->
<!--                    </li>-->
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p>Add User</p>
        </div>
        <form method="post" action="<?php echo site_url('users/create'); ?>" enctype="multipart/form-data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null no_padding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="firstname">First name:</label>
                        <input type="text" required placeholder="First name" class="form-control" name="firstname" id="firstname">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="lastname">Last name:</label>
                        <input type="text" required placeholder="Last name" class="form-control" name="lastname" id="lastname">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="username">User name:</label>
                        <input type="text" required placeholder="User name" class="form-control" name="username" id="username">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="email">Email:</label>
                        <input type="email" required placeholder="Email" class="form-control" name="email" id="email">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                            <label for="sex">Sex:</label>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                            <input type="radio" name="sex" checked value="Male" required> Male
                            <input required type="radio" name="sex" value="Female"> Female
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="password">Password:</label>
                        <input type="password" placeholder="Password" class="form-control" name="password" id="password">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="address">Address:</label>
                        <textarea name="address" id="address" class="form-control" rows="1" required></textarea>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="status">Status:</label>
                        <select required class="selectpicker form-control" id="status" name="status">
                            <option value="">--Status--</option>
                            <option value="active">Active</option>
                            <option value="inactive">In Active</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>users/view" class="btn btn-info" role="button">Back</a>
                <button type="submit" class="btn btn-primary text-center">Submit</button>
            </div>
        </form>
    </div>
</div>
