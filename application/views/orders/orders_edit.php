<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Orders Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders'); ?>">Add Order</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/view'); ?>">View Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/report'); ?>">Order Report</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/detailed_report'); ?>">Detailed Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Edit Order</span></p>
        </div>

        <?php if (isset($edit_data) && is_array($edit_data) && count($edit_data)){ ?>
        <form method="post" action="<?php echo site_url('orders/update/'); ?>" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>"/>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null quick_da">
                    <div class="form-group padding_top_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="order_name">Order Name:</label>
                        <input type="text" required placeholder="Order Name"
                               value="<?php echo $edit_data['order_name']; ?>" class="form-control" name="order_name"
                               id="order_name">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="company_id">Company Name:</label>
                        <select required class="selectpicker form-control" id="company_id" name="company_id">
                            <option value="">-- <?php if (!$all_company_data || empty($all_company_data)) {
                                    echo 'No';
                                } ?> Company Name--
                            </option>
                            <?php
                            if (isset($all_company_data)) {
                                $i = 0;
                                foreach ($all_company_data as $data) { ?>
                                    <option <?php if (intval(@$edit_data['company_id']) == intval($data['id'])) {
                                        echo 'selected';
                                    } ?> value="<?php echo $data['id'] ?>"><?php echo $data['company_name'] ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="order_qty">Order Quantity:</label>
                        <input type="text" required placeholder="Order Quantity"
                               value="<?php echo $edit_data['order_qty']; ?>" class="form-control" name="order_qty"
                               id="order_qty">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="reed_pick">Reed And Pick:</label>
                        <input type="text" required placeholder="Reed And Pick"
                               value="<?php echo $edit_data['reed_pick']; ?>" class="form-control" name="reed_pick"
                               id="reed_pick">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="cost">Cost:</label>
                        <input type="text" required placeholder="Cost" value="<?php echo $edit_data['cost']; ?>"
                               class="form-control" name="cost"
                               id="cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_count">Warp Count:</label>
                        <input type="text" required placeholder="Warp Count"
                               value="<?php echo $edit_data['warp_count']; ?>" class="form-control" name="warp_count"
                               id="warp_count">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_colours">Warp Colours:</label>
                        <select required class="selectpicker form-control" id="warp_colours" name="warp_colours">
                            <option value="">--Warp Colours--</option>
                            <option <?php if ($edit_data['warp_colours'] == '1') {
                                echo 'selected';
                            } ?> value="1">1
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '2') {
                                echo 'selected';
                            } ?> value="2">2
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '3') {
                                echo 'selected';
                            } ?> value="3">3
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '4') {
                                echo 'selected';
                            } ?> value="4">4
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '5') {
                                echo 'selected';
                            } ?> value="5">5
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '6') {
                                echo 'selected';
                            } ?> value="6">6
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '7') {
                                echo 'selected';
                            } ?> value="7">7
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '8') {
                                echo 'selected';
                            } ?> value="8">8
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '9') {
                                echo 'selected';
                            } ?> value="9">9
                            </option>
                            <option <?php if ($edit_data['warp_colours'] == '10') {
                                echo 'selected';
                            } ?> value="10">10
                            </option>
                        </select>
                    </div>

                    <span class="warp_colour_open_close_front col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                            $m = 1;
                            for ($i = 0; $i < intval($edit_data['warp_colours']); $i++) { ?>
                                <div class="form-group padding_top_ten">
                                    <label for="<?php echo 'warp_colour_' . $m ?>">Warp Colour <?php echo $m; ?>
                                        :</label>
                                    <input type="text" required placeholder="Weft Colour <?php echo $m; ?>"
                                           value="<?php echo $edit_data['warp_colour_' . $m]; ?>" class="form-control"
                                           name="<?php echo 'warp_colour_' . $m ?>"
                                           id="<?php echo 'warp_colour_' . $m; ?>">
                                 </div>
                                <?php $m++;
                            } ?>
                    </span>


                    <span class="warp_colour_open_close col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    </span>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">

                    <div class="form-group padding_top_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="loom_cost">Loom Cost:</label>
                        <input type="text" required placeholder="Loom Cost"
                               value="<?php echo $edit_data['loom_cost']; ?>" class="form-control" name="loom_cost"
                               id="loom_cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_cost">Warp Cost:</label>
                        <input type="text" required placeholder="Warp Cost"
                               value="<?php echo $edit_data['warp_cost']; ?>" class="form-control" name="warp_cost"
                               id="warp_cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="knot_cost">Knot Cost:</label>
                        <input type="text" required placeholder="Knot Cost"
                               value="<?php echo $edit_data['knot_cost']; ?>" class="form-control" name="knot_cost"
                               id="knot_cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="hook_cost">Hook Cost:</label>
                        <input type="text" required placeholder="Hook Cost"
                               value="<?php echo $edit_data['hook_cost']; ?>" class="form-control" name="hook_cost"
                               id="hook_cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="pin_cost">Pin Cost:</label>
                        <input type="text" required placeholder="Pin Cost" value="<?php echo $edit_data['pin_cost']; ?>"
                               class="form-control" name="pin_cost"
                               id="pin_cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="weft_count">Weft Count:</label>
                        <input type="text" required placeholder="Weft Count"
                               value="<?php echo $edit_data['weft_count']; ?>" class="form-control" name="weft_count"
                               id="weft_count">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="weft_colours">Weft Colours:</label>
                        <select required class="selectpicker form-control weft_colours" id="weft_colours"
                                name="weft_colours">
                            <option value="">--Weft Colours--</option>
                            <option <?php if ($edit_data['weft_colours'] == '1') {
                                echo 'selected';
                            } ?> value="1">1
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '2') {
                                echo 'selected';
                            } ?> value="2">2
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '3') {
                                echo 'selected';
                            } ?> value="3">3
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '4') {
                                echo 'selected';
                            } ?> value="4">4
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '5') {
                                echo 'selected';
                            } ?> value="5">5
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '6') {
                                echo 'selected';
                            } ?> value="6">6
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '7') {
                                echo 'selected';
                            } ?> value="7">7
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '8') {
                                echo 'selected';
                            } ?> value="8">8
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '9') {
                                echo 'selected';
                            } ?> value="9">9
                            </option>
                            <option <?php if ($edit_data['weft_colours'] == '10') {
                                echo 'selected';
                            } ?> value="10">10
                            </option>
                        </select>
                    </div>

                    <span class="weft_colour_open_close col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    </span>

                    <span class="weft_colour_open_close_front col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                            $m = 1;
                            for ($i = 0; $i < intval($edit_data['weft_colours']); $i++) { ?>
                                <div class="form-group padding_top_ten">
                                    <label for="<?php echo 'weft_colour_' . $m ?>">Weft Colour <?php echo $m; ?>
                                        :</label>
                                    <input type="text" required placeholder="Weft Colour <?php echo $m; ?>"
                                           value="<?php echo $edit_data['weft_colour_' . $m]; ?>" class="form-control"
                                           name="<?php echo 'weft_colour_' . $m ?>"
                                           id="<?php echo 'weft_colour_' . $m; ?>">
                                 </div>
                                <?php $m++;
                            } ?>
                    </span>

                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_bottom_ten">
                    <label for="status">Status:</label>
                    <select required class="selectpicker form-control" id="status" name="status">
                        <option value="">--Status--</option>
                        <option <?php if ($edit_data['status'] == 'active') {
                            echo 'selected';
                        } ?> value="active">Active
                        </option>
                        <option <?php if ($edit_data['status'] == 'inactive') {
                            echo 'selected';
                        } ?> value="inactive">In Active
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding_left_null padding_top_bottom pull-left">
                            <label class="pull-left">Employee Photo</label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding_top_bottom">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php if (file_exists('uploads/orders/' . $edit_data['order_image']) && $edit_data['order_image']) { ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4 change_picture col-xs-12">
                                        <img class="imgInp"
                                             src="<?php echo site_url() ?>uploads/orders/<?php echo $edit_data['order_image'] ?>"
                                             alt="order_img" title="order_img">
                                    </div>
                                <?php } ?>
                                <input type="file" id="imgInp" name="userfile" size="20"/>
                                <input type="hidden" value="<?php if ($edit_data['order_image']) {
                                    echo $edit_data['order_image'];
                                } ?>" id="imgInp" name="UserImageFile" size="20"/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 alert alert-warning padding_top_bottom">
                            <strong>Warning!</strong> Allowed files types gif,jpg,png
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>orders/view" class="btn btn-info" role="button">Back</a>
                <button type="submit" class="btn btn-primary text-center">Submit</button>
            </div>

            <?php } ?>
        </form>
    </div>
</div>
