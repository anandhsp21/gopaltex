<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Orders Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders'); ?>">Add Order</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/view'); ?>">View Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/report'); ?>">Order Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/detailed_report'); ?>">Detailed Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">View Order</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title margin_top_ten col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_right_null padding_left_null">
                <form method="post" action="<?php echo base_url(); ?>orders/view">
                    <div class="form-group search_button col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 reset_empty">
                            <input type="text" placeholder="Order Name" class="form-control" id="order_name"
                                   name="order_name"
                                   value="<?php if (!empty($order_title)) {
                                       echo $order_title;
                                   } ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input type="submit" name="search" class="btn btn-primary">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input type="button" value="Reset" class="reset_button btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom table_over_flow padding_right_null padding_left_null">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Order Name</th>
                        <th>Company Name</th>
                        <th>Order Qty</th>
                        <th>Reed Pick</th>
                        <th>Cost</th>
                        <th>Order Image</th>
                        <th>Status</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (isset($datas) && is_array($datas) && count($datas)) {
                        $i = 0;
                        foreach ($datas as $data) {
                            ?>
                            <tr>
                                <td><?php echo $data["id"]; ?></td>
                                <td><?php echo $data["order_name"]; ?></td>
                                <td>
                                <?php if(isset($company_ids) && is_array($company_ids) && count($company_ids)){
                                    foreach ($company_ids as $company_id) {
                                        if(intval($company_id['id']) == intval($data['company_id'])){
                                            echo $company_id["company_name"];
                                        }
                                    }
                                } ?>
                                </td>
                                <td><?php echo $data["order_qty"]; ?></td>
                                <td><?php echo $data["reed_pick"]; ?></td>
                                <td><?php echo $data["cost"]; ?></td>
                                <td class="emp_view_profile">
                                    <?php if ($data['order_image']) { ?>
                                        <img src="<?php echo base_url() ?>/uploads/orders/<?php echo $data["order_image"]; ?>"
                                             alt="<?php echo $data["order_image"]; ?>"
                                             title="<?php echo $data["order_image"]; ?>">
                                    <?php } else { ?>
                                        No Order Picture
                                    <?php } ?>
                                </td>
                                <td><?php echo $data["status"]; ?></td>
                                <td><a href="<?php echo site_url('orders/edit/' . $data['id'] . ''); ?>">View</a>
                                </td>
                                <td><a href="<?php echo site_url('orders/delete/' . $data['id'] . ''); ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <?php if (!$datas) { ?>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Order found</p>
                <?php  } ?>
            </div>
            <div class="pagination-dive col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $nav; ?>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
            <a href="<?php echo base_url(); ?>orders" class="btn btn-info" role="button">Add New Order</a>
        </div>
    </div>
</div>
