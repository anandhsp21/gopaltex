<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Orders Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders'); ?>">Add Orders</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/view'); ?>">View Orders</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/report'); ?>">Orders Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/detailed_report'); ?>">Detailed Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Add Order</span></p>
        </div>

        <form  method="post" action="<?php echo site_url('orders/create'); ?>" enctype="multipart/form-data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null quick_dashboard_mobile_right">
                    <div class="form-group padding_top_ten">
                        <label for="order_name">Order Name:</label>
                        <input type="text" required placeholder="Order Name" class="form-control" name="order_name"
                               id="order_name">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="company_id">Company Name:</label>
                        <select class="selectpicker form-control required" id="company_id" name="company_id">
                            <option value="">-- <?php if(!$all_company_data || empty($all_company_data)){ echo 'No'; } ?> Company Name--</option>
                            <?php
                            if (isset($all_company_data)) {
                                $i = 0;
                                foreach ($all_company_data as $data) { ?>
                                    <option value="<?php echo $data['id']; ?>"><?php echo $data['company_name'] ?></option>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </select>


                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="order_qty">Order Quantity:</label>
                        <input type="text" required placeholder="Order Quantity" class="form-control" name="order_qty"
                               id="order_qty">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="reed_pick">Reed And Pick:</label>
                        <input type="text" required placeholder="Reed And Pick" class="form-control" name="reed_pick"
                               id="reed_pick">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="cost">Cost:</label>
                        <input type="text" required placeholder="Cost" class="form-control" name="cost"
                               id="cost">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_count">Warp Count:</label>
                        <input type="text" required placeholder="Warp Count" class="form-control" name="warp_count"
                               id="warp_count">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_colours">Warp Colours:</label>
                        <select required class="selectpicker form-control" id="warp_colours" name="warp_colours">
                            <option value="">--Warp Colours--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <span class="warp_colour_open_close">

                    </span>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_top_ten padding_bottom_ten padding_left_null padding_right_null">

                    <div class="form-group padding_top_ten col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="loom_cost">Loom Cost:</label>
                        <input type="text" required placeholder="Loom Cost" class="form-control" name="loom_cost"
                               id="loom_cost">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="warp_cost">Warp Cost:</label>
                        <input type="text" required placeholder="Warp Cost" class="form-control" name="warp_cost"
                               id="warp_cost">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="knot_cost">Knot Cost:</label>
                        <input type="text" required placeholder="Knot Cost" class="form-control" name="knot_cost"
                               id="knot_cost">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="hook_cost">Hook Cost:</label>
                        <input type="text" required placeholder="Hook Cost" class="form-control" name="hook_cost"
                               id="hook_cost">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="pin_cost">Pin Cost:</label>
                        <input type="text" required placeholder="Pin Cost" class="form-control" name="pin_cost"
                               id="pin_cost">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="weft_count">Weft Count:</label>
                        <input type="text" required placeholder="Weft Count" class="form-control" name="weft_count"
                               id="weft_count">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="weft_colours">Weft Colours:</label>
                        <select required class="selectpicker form-control weft_colours" id="weft_colours" name="weft_colours">
                            <option value="">--Weft Colours--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <span class="weft_colour_open_close">

                    </span>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_ten padding_right_ten padding_bottom_ten">
                   <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
                       <label for="status">Status:</label>
                       <select required class="selectpicker form-control" id="status" name="status">
                           <option value="">--Status--</option>
                           <option value="active">Active</option>
                           <option value="inactive">In Active</option>
                       </select>
                   </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_ten padding_right_ten padding_bottom_ten">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_left_null padding_top_bottom pull-left">
                                <label class="pull-left">Design Attachment</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_bottom">
                                <input type="file" name="userfile" size="20"/>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 alert alert-warning padding_top_bottom">
                                <strong>Warning!</strong> Allowed files types gif,jpg,png
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">
                <a href="<?php echo base_url(); ?>orders/view" class="btn btn-info" role="button">Back</a>
                <button type="submit" class="btn btn-primary text-center">Submit</button>
            </div>
        </form>
    </div>
</div>
