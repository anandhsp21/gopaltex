<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid quick_dashboard">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 container-fluid quick_dashboard_left_menu">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Orders Management</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <nav class="navbar bg-light col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="navbar-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <li class="nav-item ">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders'); ?>">Add Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/view'); ?>">View Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/report'); ?>">Order Report</a>
                    </li>
                    <li class="nav-item active">
                        <a class="wow fadeIn animated nav-link" href="<?php echo site_url('orders/detailed_report'); ?>">Detailed Report</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 container-fluid padding_right_null quick_dashboard quick_dashboard_mobile">
        <div class="col-lg-12 col-md-12 col-sm-12 quick_menu_title col-xs-12">
            <p><span class="wow fadeInDown animated">Detailed View Order</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom no_padding">
            <form method="post" action="<?php echo site_url('orders/detailed_report_pdf'); ?>" enctype="multipart/form-data">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding padding_left_null padding_right_null">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_top_ten padding_bottom_ten quick_dashboard_mobile quick_dashboard_mobile_right">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="order_id">Order Name:</label>
                                <select required class="selectpicker form-control" id="order_id" name="order_id">
                                    <option value="">-- <?php if (!$all_order_data || empty($all_order_data)) {
                                            echo 'No';
                                        } ?> Order Name--
                                    </option>
                                    <?php
                                    if (isset($all_order_data)) {
                                        $i = 0;
                                        foreach ($all_order_data as $data) { ?>
                                            <option value="<?php echo $data['id'] ?>"><?php echo $data['order_name'] ?></option>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center padding_left_null padding_right_null">
                    <button type="submit" class="btn btn-primary text-center">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>