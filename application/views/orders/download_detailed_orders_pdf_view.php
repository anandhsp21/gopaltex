<div class="col-md-12">
    <div class="col-md-4">
        <p style="text-align: left;font-weight: bold;font-style: italic;">Gopal Tex</p>
    </div>
    <div style="text-align: center" class="col-md-4">
        <img style="width: 25%" src="<?php echo site_url() ?>skin/image/gopaltex.png" alt="gopal-tex"
             title="gopal-tex"/>
    </div>
    <div class="col-md-4">
        <p style="text-align: right;font-weight: bold;font-style: italic;"><?php echo date("d/m/Y"); ?></p>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-4">
        <p style="text-align: left;font-weight: bold"><?php echo $company_order_name['company_name']; ?></p>
    </div>
    <div class="col-md-4">
        <p style="text-align: center;font-weight: bold"><?php echo $title; ?></p>
    </div>
    <div class="col-md-4">
        <p style="text-align: right;font-weight: bold"><?php echo $company_order_name['order_name']; ?></p>
    </div>
</div>

<div class="col-md-12 margin_top_ten">
    <?php if ($datas) { ?>
        <table id="customers" class="col-md-12 margin_top_ten">
            <thead>
            <tr>
                <th>Id</th>
                <th>Employee Name</th>
                <th>Order Name</th>
                <th>Company Name</th>
                <th>Date</th>
                <th>order qty</th>
                <th>Meters</th>
                <th>Loom No</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $final_metres = 0;
            $total_metres = 0;
            $order_status = '';
            if (isset($datas) && is_array($datas) && count($datas)) {
                $i = 0;
                foreach ($datas as $data) {
//                    var_dump($data);die;
                    $final_metres += $data["meters"];
                    $total_metres = $data["order_qty"];
                    $order_status = $data["order_status"];
                    ?>
                    <tr>
                        <td><?php echo $data["id"]; ?></td>
                        <td><?php echo $data["emp_name"]; ?></td>
                        <td><?php echo $data["order_name"]; ?></td>
                        <td><?php echo $data["company_name"]; ?></td>
                        <td><?php echo $data["date"]; ?></td>
                        <td><?php echo $data["order_qty"]; ?></td>
                        <td><?php echo $data["meters"]; ?></td>
                        <td><?php echo $data["loom_no"]; ?></td>
                    </tr>
                    <?php
                    $i++;
                }
            }
            ?>
            </tbody>
        </table>
        <div class="col-md-12 margin_top_ten">
            <div class="col-md-4">
                <p>&nbsp;</p>
            </div>
            <div class="col-md-4">
                <p>&nbsp;</p>
            </div>
            <div class="col-md-4">
                <p>Total Metres : <?php echo $total_metres; ?></p>
                <p>Available Metres : <?php echo $final_metres; ?></p>
                <p>Remaining Metres : <?php echo $total_metres - $final_metres; ?></p>
                <p>Order status : <?php echo $order_status; ?></p>
            </div>
        </div>

    <?php } ?>

    <?php if (!$datas) { ?>
        <p class="col-md-12">No Entries found</p>
    <?php } ?>

</div>

<div class="col-md-12 text-center margin_top_ten">
    <a href="<?php echo base_url(); ?>orders/detailed_report_pdf" class="btn btn-info" role="button">Back</a>
</div>

<style>
    .col-md-12 {
        width: 100%;
        float: left;
    }

    .col-md-12 {
        width: 100%;
        float: left;
    }

    .col-md-6 {
        width: 50%;
        float: left;
    }

    .col-md-4 {
        width: 33.33%;
        float: left;
    }

    .col-md-3 {
        width: 25%;
        float: left;
    }

    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #D2C3C3;
        color: white;
    }

    .text-center {
        text-align: center;
    }

    .margin_top_ten {
        margin-top: 10px;
    }

    .margin_bottom_ten {
        margin-bottom: 10px;
    }

    .margin_top_bottom {
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>