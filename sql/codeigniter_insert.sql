-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2017 at 02:26 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codeigniter_insert`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments_data`
--

CREATE TABLE `comments_data` (
  `id` int(255) NOT NULL,
  `document_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `role_id` int(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `likes_data`
--

CREATE TABLE `likes_data` (
  `id` int(255) NOT NULL,
  `document_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `role_id` int(255) NOT NULL,
  `likes_count` int(255) NOT NULL DEFAULT '0',
  `status` varchar(100) NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_data`
--

CREATE TABLE `users_data` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `created_at` timestamp(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE `user_data` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `sex` varchar(150) NOT NULL,
  `address` varchar(250) NOT NULL,
  `job_destination` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(255) NOT NULL,
  `created_date` timestamp(5) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id`, `firstname`, `lastname`, `username`, `email`, `sex`, `address`, `job_destination`, `pass`, `password`, `role_id`, `created_date`, `status`) VALUES
(13, 'admin', 'admin', 'admin', 'admin@gmail.com', 'Male', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 0, '0000-00-00 00:00:00.00000', 'Y'),
(17, 'Anandh', 'sp', 'anandhsp21', 'anandhsp21@gmail.com', 'Male', 'Namakkal', 'web developer', 'f4cf57b5efd225d8c262b3f062a464d0', '235696', 1, '0000-00-00 00:00:00.00000', 'Y'),
(18, 'Gopal', 'sp', 'gopalsp21', 'gopal@gmail.com', 'Female', 'Namakkal', 'web developer', 'f4cf57b5efd225d8c262b3f062a464d0', '235696', 1, '0000-00-00 00:00:00.00000', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments_data`
--
ALTER TABLE `comments_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes_data`
--
ALTER TABLE `likes_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_data`
--
ALTER TABLE `users_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments_data`
--
ALTER TABLE `comments_data`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `likes_data`
--
ALTER TABLE `likes_data`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `users_data`
--
ALTER TABLE `users_data`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `user_data`
--
ALTER TABLE `user_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
